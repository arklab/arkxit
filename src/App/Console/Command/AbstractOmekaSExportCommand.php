<?php

/**
 * ARK Console Command.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace App\Console\Command;

use ARK\ARK;
use ARK\DBAL\Console\Command\DatabaseCommand;
use ARK\Spatial\Projector\Proj4Projector;
use Brick\Geo\Geometry;
use Brick\Geo\GeometryCollection;
use DateTime;
use Doctrine\DBAL\ArrayParameterType;
use Throwable;
use Symfony\Component\HttpClient\HttpClient;

abstract class AbstractOmekaSExportCommand extends DatabaseCommand
{
    protected $omekas = [
        'url' => '',
        'credentials' => [],
        'vocabularies' => [],
        'resource_templates' => [],
        'properties' => [],
    ];

    protected $templates = [];

    protected $geonames = [
        'url' => '',
        'credentials' => [
            'username' => '',
        ],
    ];

    protected $types = [
        'txt' => [
            'tbl' => 'cor_tbl_txt',
            'lut' => 'cor_lut_txttype',
            'type' => 'txttype',
        ],
        'number' => [
            'tbl' => 'cor_tbl_number',
            'lut' => 'cor_lut_numbertype',
            'type' => 'numbertype',
        ],
        'date' => [
            'tbl' => 'cor_tbl_date',
            'lut' => 'cor_lut_datetype',
            'type' => 'datetype',
        ],
        'action' => [
            'tbl' => 'cor_tbl_action',
            'lut' => 'cor_lut_actiontype',
            'type' => 'actiontype',
        ],
        'span' => [
            'tbl' => 'cor_tbl_span',
            'lut' => 'cor_lut_spantype',
            'type' => 'spantype',
        ],
        'attribute' => [
            'tbl' => 'cor_tbl_attribute',
            'lut' => 'cor_lut_attributetype',
            'type' => 'attributetype',
        ],
        'spatial' => [
            'tbl' => 'cor_tbl_spatial',
            'lut' => null,
            'type' => 'geomtype',
        ],
        'file' => [
            'tbl' => 'cor_tbl_file',
            'lut' => 'cor_lut_filetype',
            'type' => 'filetype',
        ],
    ];

    // List of Attribute Types to convert to SKOS Scheme/Concept Items
    protected $vocabularies = [
        'site' => [
            'example_site' => [
                'type' => [
                    'ste_cd',
                ],
                'collection' => 'Site Example Collection',
                'customvocab' => 'Site Example Vocabulary',
                'scheme' => 'Site Example Scheme',
                'notation' => 'example_site',
                'is_public' => 1,
                'owner' => 1,
            ],
        ],
        'attribute' => [
            'example_attr' => [
                'type' => 'example_type',
                'collection' => 'Attribute Example Collection',
                'customvocab' => 'Attribute Example Vocabulary',
                'notation' => 'example_attr',
                'is_public' => 1,
                'owner' => 1,
            ],
        ],
        'action' => [
            'example_action' => [
                'type' => [
                    'action',
                ],
                'collection' => 'Action Example Collection',
                'customvocab' => 'Action Example Vocabulary',
                'scheme' => 'Action Example Scheme',
                'notation' => 'example_action',
                'is_public' => 1,
                'owner' => 1,
            ],
        ],
    ];

    protected $core = [
        'item' => [
            'property' => 'ontology:item',
        ],
        'item_register' => [
            'property' => 'ontology:item_register',
            'customvocab' => '1',
            'values' => [
                'ste_cd' => 'Vocabulary',
            ],
        ],
        'item_sequence' => [
            'property' => 'ontology:item_sequence',
        ],
    ];

    protected $modules = [
        'abk' => [
            'key' => [
                'tbl' => 'abk_tbl_abk',
                'lut' => 'abk_lut_abktype',
                'mod_cd' => 'abk_cd',
                'mod_no' => 'abk_no',
                'type' => 'abktype',
            ],
            'core' => true,
            'select' => [
                'tbl' => [
                    'abk_cd' => 'item',
                    'ste_cd' => 'item_register',
                    'abk_no' => 'item_sequence',
                    'cre_on' => 'created',
                    'omekas_id' => 'id',
                    'omekas_updated' => 'updated',
                ],
                'lut' => [
                    'abktype' => 'type',
                ],
            ],
            'modtype' => [
                'people' => [
                    'resource_template_name' => 'Person',
                    'resource_class_name' => 'ontology:Person',
                    'item_sets' => [
                        'Actors',
                    ],
                    'sites' => [
                        'Site',
                    ],
                    'datatype' => [
                        'txt' => [
                            'name' => [
                                'property' => 'ontology:name',
                                'language' => true,
                            ],
                        ],
                    ],
                ],
                'organisation' => [
                    'resource_template_name' => 'Organisation',
                    'resource_class_name' => 'ontology:Organisation',
                    'item_sets' => [
                        'Actors',
                    ],
                    'sites' => [
                        'Site',
                    ],
                    'datatype' => [
                        'txt' => [
                            'name' => [
                                'property' => 'ontology:name',
                                'language' => true,
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    protected $client;
    protected $timestamp;
    protected $path = '';
    protected $projector;

    // OVERRIDE THIS
    protected function configure() : void
    {
        $this->setCommandOptions('ark:export', 'Export ARK v1 to Omeka S JSON-LD API');
    }

    protected function setCommandOptions(string $name, string $description) : void
    {
        $modules = implode(',', array_keys($this->modules));
        $this->setName($name)
            ->setDescription($description)
            ->addOptionValue('modules', 'Comma-separated list of Modules to export Items', $modules)
            ->addOptionValue('media', 'Comma-separated list of Modules to export Media', '')
            ->addOptionValue('items', 'Comma-separated list of Items to export', '')
            ->addOptionValue('start', 'Item to start export from, must have set a single Module', '')
            ->addOptionValue('registrars', 'Comma-separated list of Registrars to export', '')
            ->addOptionBoolean('skos', 'Only export Attributes to SKOS Items')
            ->addOptionBoolean('all', 'Export all Items, otherwise only export new Items');
    }

    protected function doExecute() : int
    {
        // If passed a list of modules, check they are valid and compose into correct process order
        $modules = [];
        $mods = explode(',', $this->getOption('modules'));
        foreach (array_keys($this->modules) as $mod) {
            if (in_array($mod, $mods)) {
                $modules[] = $mod;
            }
        }

        // If passed a list of media modules, check they are valid and compose into correct process order
        $media = [];
        $meds = explode(',', $this->getOption('media'));
        foreach (array_keys($this->modules) as $mod) {
            if (in_array($mod, $meds)) {
                $media[] = $mod;
            }
        }

        // Get Items flag
        $items = $this->getOption('items');
        $items = $items ? explode(',', $items) : [];

        // Get Start flag, check required Module is set
        $start = $this->getOption('start');
        if ($start and (count($modules) > 1 or count($media) > 1)) {
            $this->write('If start flag is set must specify only 1 module to process.');
            return 0;
        }

        // Get Registrars flag
        $registrars = $this->getOption('registrars');
        $registrars = $registrars ? explode(',', $registrars) : [];

        // Get the all items flag
        $this->all = $this->getOption('all');

        // Get the all items flag
        $skos = $this->getOption('skos');

        // Priority 1: Process select items, ignore the rest
        if ($items) {
            if ($start) {
                $start = '';
                $this->write('Ignoring start flag as items flag is set.');
            }
            if ($registrars) {
                $registrars = [];
                $this->write('Ignoring registrars flag as items flag is set.');
            }
            if ($this->all) {
                $this->all = false;
                $this->write('Ignoring all flag as items flag is set.');
            }
        }

        // Proirity 2: Process starting from selected item, ignore the rest
        if ($start) {
            if ($registrars) {
                $registrars = [];
                $this->write('Ignoring registrars flag as start flag is set.');
            }
            if ($this->all) {
                $this->all = false;
                $this->write('Ignoring all flag as start flag is set.');
            }
        }

        // Priority 3: Process all items for Registrar
        if ($registrars) {
            if ($this->all) {
                $this->all = false;
                $this->write('Ignoring all flag as registrars flag is set.');
            }
        }

        // Priority 4: Process all items
        // Priority 5: Otherwise Process just new Items

        $this->client = HttpClient::create();

        $this->path = $_ENV['ARK1_PATH'];
        $this->loadMapping();
        $this->loadOmekaS();
        $this->loadGeonames();
        $this->loadCustom();

        $this->timestamp = ARK::timestamp();

        $this->exportUsers();

        // Export the Attributes as SKOS vocabularies then exit
        if ($skos) {
            if (!$this->exportVocabularies()) {
                $this->write('FAILED VOCABULARY PROCESSING!!!');
            }
            return 0;
        }

        foreach ($modules as $mod) {
            if (!$this->exportModule($mod, $items, $start, $registrars)) {
                $this->write('FAILED MODULE PROCESSING!!!');
                return 0;
            }
        }

        foreach ($media as $mod) {
            if (!$this->exportMedia($mod, $items, $start, $registrars)) {
                $this->write('FAILED MEDIA PROCESSING!!!');
                return 0;
            }
        }

        return 0;
    }

    // Load the ARK to Omeka S mapping
    protected function loadMapping() : void
    {
        // Add the core Item fields to the mapping if required, but allow for overrides e.g. for type
        foreach ($this->modules as $mod => $module) {
            if ($module['core']) {
                foreach ($module['modtype'] as $modtype => $modtypemap) {
                    $mapping = array_merge($this->core, $modtypemap['cor'] ?? []);
                    $this->modules[$mod]['modtype'][$modtype]['datatype']['cor'] = $mapping;
                }
            }
        }
    }

    // Fetch all properites from OmekaS install
    protected function loadOmekaS() : void
    {
        $this->write('');
        $this->write('Loading Omeka S Config from API');

        // Load the credentials
        $this->omekas['url'] = $_ENV['OMEKAS_API_URL'];
        $this->omekas['credentials']['key_identity'] = $_ENV['OMEKAS_API_IDENTITY'];
        $this->omekas['credentials']['key_credential'] = $_ENV['OMEKAS_API_CREDENTIAL'];

        // Fetch the Omeka S Classes
        $resources = $this->getOmekaSResources('resource_classes', ['per_page'=>10000])->toArray();
        foreach ($resources as $resource) {
            $this->omekas['resource_classes'][$resource['o:id']] = $resource;
            $this->omekas['resource_classes'][$resource['o:term']] = $resource;
        }

        // Fetch the Omeka S Resource Templates
        $resources = $this->getOmekaSResources('resource_templates', ['per_page'=>10000])->toArray();
        foreach ($resources as $resource) {
            $this->omekas['resource_templates'][$resource['o:id']] = $resource;
            $this->omekas['resource_templates'][$resource['o:label']] = $resource;
        }

        // Fetch the Omeka S Vocabulary and Property IDs, aka Ontologies in the real world
        $vocabs = $this->getOmekaSResources('vocabularies', ['per_page'=>10000])->toArray();
        foreach ($vocabs as $vocab) {
            $this->omekas['vocabularies'][$vocab['o:id']] = $vocab;
        }
        $props = $this->getOmekaSResources('properties', ['per_page'=>10000])->toArray();
        foreach ($props as $prop) {
            $vocab = $prop['o:vocabulary']['o:id'];
            $this->omekas['vocabularies'][$vocab]['properties'][$prop['o:id']] = $prop;
            $this->omekas['properties'][$prop['o:id']] = $prop;
            $this->omekas['properties'][$prop['o:term']] = $prop;
        }

        // Fetch the Custom Vocabs and the member Items
        $customs = $this->getOmekaSResources('custom_vocabs', ['per_page'=>10000])->toArray();
        foreach ($customs as $custom) {
            $search = [
                'item_set_id' => [$custom['o:item_set']['o:id']],
                // TODO I think this is for Concepts only, exclude Schemes? Find via lookup!
                'resource_class_id' => 212,
                'per_page'=>10000,
            ];
            $items = $this->getOmekaSResources('items', $search)->toArray();
            $custom['items'] = $items;
            foreach ($items as $item) {
                $custom['concepts'][$item['o:title']] = $item['o:id'];
            }
            $this->omekas['custom_vocabs'][$custom['o:id']] = $custom;
            $this->omekas['custom_vocabs'][$custom['o:label']] = $custom;
        }

        // Fetch the Omeka S Item Sets
        $sets = $this->getOmekaSResources('item_sets', ['per_page'=>10000])->toArray();
        foreach ($sets as $set) {
            $this->omekas['item_sets'][$set['o:id']] = $set;
            $this->omekas['item_sets'][$set['o:title']] = $set;
        }

        // Fetch the Omeka S Sites
        $sites = $this->getOmekaSResources('sites', ['per_page'=>10000])->toArray();
        foreach ($sites as $site) {
            $this->omekas['sites'][$site['o:id']] = $site;
            $this->omekas['sites'][$site['o:title']] = $site;
        }

        // Build the resource templates to use in exporting to Omeka S JSON-LD
        foreach ($this->omekas['resource_templates'] as $resource) {
            // Skip the Base Resource which we don't need
            if ($resource['o:id'] === 1) {
                continue;
            }
            $name = $resource['o:label'];
            $template = [];
            $template['resource_template_id'] = $resource['o:id'];
            if (array_key_exists('o:resource_class', $resource) && is_array($resource['o:resource_class'])) {
                $template['resource_class_id'] = $resource['o:resource_class']['o:id'];
            }
            $template['is_public'] = true;
            $template['owner'] = $resource['o:owner']['o:id'];
            $template['properties'] = [];
            foreach ($resource['o:resource_template_property'] as $prop) {
                $prop_id = $prop['o:property']['o:id'];
                $property = $this->omekas['properties'][$prop_id];
                $prop_term = $property['o:term'];
                $template['properties'][$prop_term] = [];
                $template['properties'][$prop_term]['property_id'] = $prop_id;
                $template['properties'][$prop_term]['type'] = $prop['o:data_type'][0] ?? 'literal';
                $template['properties'][$prop_term]['is_public'] = !$prop['o:is_private'] ?? true;
                $template['properties'][$prop_term]['multiple'] = (($prop['o:data'][0]['max_values'] ?? '') != '1');
            }
            $this->templates[$name] = $template;
        }

        // Pre-build the JSON-LD value templates to save time
        foreach ($this->modules as $mod => $module) {
            foreach ($module['modtype'] as $modtype => $modtypemap) {
                $resource = $this->templates[$modtypemap['resource_template_name']];
                $mapping = array_merge($resource, $modtypemap);
                foreach ($modtypemap['datatype'] as $datatype => $datatypemap) {
                    foreach ($datatypemap as $field => $fieldmap) {
                        if (isset($fieldmap['property'])) {
                            $property = $resource['properties'][$fieldmap['property']];
                            $fieldmap['multiple'] = $property['multiple'];
                            $fieldmap['value']['type'] = $property['type'];
                            $fieldmap['value']['property_id'] = $property['property_id'];
                            $fieldmap['value']['is_public'] = $property['is_public'];
                            if (isset($fieldmap['annotation'])) {
                                $annomap = $fieldmap['annotation'];
                                $anno_resource = $this->templates[$annomap['resource_template_name']];
                                $annotation = $anno_resource['properties'][$annomap['property']];
                                $annomap['multiple'] = $annotation['multiple'];
                                $annomap['value']['type'] = $annotation['type'];
                                $annomap['value']['property_id'] = $annotation['property_id'];
                                $annomap['value']['is_public'] = $annotation['is_public'];
                                $fieldmap['annotation'] = $annomap;
                            }
                            $mapping['datatype'][$datatype][$field] = $fieldmap;
                        }
                    }
                }
                $this->modules[$mod]['modtype'][$modtype] = $mapping;
            }
        }
    }

    // Create a simple property value from the property config
    protected function makePropertyValue(string $term, string $type, $value, string $language = null) : array
    {
        $template['property_id'] = $this->omekas['properties'][$term]['o:id'];
        $template['type'] = $type;
        if (str_starts_with($type, 'resource')) {
            $template['value_resource_id'] = $value;
        } else {
            $template['@value'] = $value;
        }
        if ($language) {
            $template['@language'] = $language;
        }
        return $template;
    }

    // Configure the Geonames API if used
    protected function loadGeonames() : void
    {
        $this->geonames['url'] = $_ENV['GEONAMES_API_URL'] ?? 'http://api.geonames.org';
        $this->geonames['credentials']['username'] = $_ENV['GEONAMES_API_USERNAME'] ?? 'demo';
    }

    // Overload to do custom config
    protected function loadCustom() : void
    {
    }

    // Export the User list to Omeka S
    protected function exportUsers() : void
    {
        $this->write('');
        $this->write('Exporting Users');
        $count = 0;
        $created = 0;
        $updated = 0;
        $this->connection()->beginTransaction();
        $qbs = $this->connection()->createQueryBuilder();
        $qbs->select('users.*')
            ->from('cor_tbl_users', 'users')
            ->orderBy('id', 'ASC');
        $users = $qbs->execute()->fetchAll();
        $total = count($users);
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update('cor_tbl_users', 'users')
            ->where('users.id = :id')
            ->set('users.omekas_id', ':omekas_id')
            ->set('users.omekas_updated', ':omekas_updated');
        foreach ($users as $user) {
            $value = [];
            // Only update if user doesn't already exist, as may be modifed other end.
            if (!isset($user['omekas_id'])) {
                $value['o:name'] = $user['firstname'].' '.$user['lastname'];
                $value['o:email'] = $user['email'];
                $value['o:role'] = 'researcher';
                $value['o:created'] = $user['cre_on'];
                $value['o:is_active'] = false;
                $response = $this->createOmekaSResource('users', $value);
                if ($response->getStatusCode() == 200) {
                    $created ++;
                    $result = $response->toArray();
                    $qbu->setParameter('id', $user['id']);
                    $qbu->setParameter('omekas_id', $result['o:id']);
                    $qbu->setParameter('omekas_updated', $this->omekaSDateToTimestamp($result['o:created']));
                    $update = $qbu->execute();
                    if ($response->getStatusCode() != 200) {
                        $this->write('UPDATE ARK USER FAILED!');
                        return;
                    } else {
                        $updated ++;
                    }
                    $count ++;
                    if ($count % 10 == 0) {
                        $this->write('    ... '.$count.' new = '.$created.' mod = '.$updated);
                    }
                } else {
                    $this->write('CREATE OMEKA S USER FAILED!');
                    $this->write('    Status Code = '.$response->getStatusCode());
                    dump($response->getContent());
                }
            }
        }
        $this->connection()->commit();
        unset($users);
        $this->write("    Processd $total ARK Users");
        $this->write("    Created $created Users in Omeka");
        $this->write("    Updated $updated Users in ARK");
    }

    // Export the Vocabularies to Omeka S
    protected function exportVocabularies() : bool
    {
        return $this->exportSiteVocabularies()
            && $this->exportActionVocabularies()
            && $this->exportAttributeVocabularies();
    }

    // Export the Attribute Vocabularies to Omeka S
    protected function exportAttributeVocabularies() : bool
    {
        $this->write('');
        $this->write('Exporting Attribute Vocabularies');

        // Fetch Attribute Type
        $qbs = $this->connection()->createQueryBuilder();
        $qbs->select('scheme.*')
            ->from('cor_lut_attributetype', 'scheme')
            ->where('scheme.attributetype = :attributetype');

        // Fetch Attribute
        $qbc = $this->connection()->createQueryBuilder();
        $qbc->select('concept.*')
            ->from('cor_lut_attributetype', 'scheme')
            ->leftJoin('scheme', 'cor_lut_attribute', 'concept', 'concept.attributetype = scheme.id')
            ->where('scheme.attributetype = :attributetype');

        // Update Attribute Type
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update('cor_lut_attributetype', 'typ')
            ->where('typ.id = :id')
            ->set('typ.omekas_id', ':omekas_id')
            ->set('typ.omekas_collection_id', ':omekas_collection_id')
            ->set('typ.omekas_customvocab_id', ':omekas_customvocab_id')
            ->set('typ.omekas_updated', ':omekas_updated');

        // Update Attribute
        $qba = $this->connection()->createQueryBuilder();
        $qba->update('cor_lut_attribute', 'attr')
            ->where('attr.id = :id')
            ->set('attr.omekas_id', ':omekas_id')
            ->set('attr.omekas_updated', ':omekas_updated');

        foreach ($this->vocabularies['attribute'] as $vocabulary => $settings) {
            $this->write('    Exporting '.$vocabulary);

            $this->connection()->beginTransaction();

            $qbs->setParameter('attributetype', $settings['type']);
            $type = $qbs->execute()->fetchAssociative();
            if (!$type) {
                $this->write('    ERROR FETCHING ATTRIBUTE TYPE');
                return false;
            }

            // Create/Update the Collection Item Set
            $collection = $this->postSkosCollection(
                $type['omekas_collection_id'] ?? null,
                $settings['collection'],
                $settings['is_public'],
                $settings['owner'],
                $type['cre_on']
            );
            if (!$collection) {
                $this->write('    FAILED CREATING VOCABULARY COLLECTION');
                return false;
            }
            $collection_id = $collection['o:id'];

            // Create/Update the Custom Vocab
            $customvocab = $this->postCustomVocabulary(
                $type['omekas_customvocab_id'] ?? null,
                $settings['customvocab'],
                $collection['o:id'],
                $settings['is_public'],
                $settings['owner']
            );
            if (!$customvocab) {
                $this->write('    FAILED CREATING CUSTOM VOCABULARY');
                return false;
            }

            // Create/Update the SKOS Scheme item
            $labels = $this->fetchAliases('cor_lut_attributetype', $type['id']);
            $scheme = $this->postSkosScheme(
                $type['omekas_id'] ?? null,
                $collection['o:id'],
                $labels,
                $settings['notation'],
                $settings['is_public'],
                $settings['owner'],
                $type['cre_on']
            );
            if (!$scheme) {
                $this->write('    FAILED CREATING VOCABULARY SCHEME');
                return false;
            }

            // Update the Atrribute Type record with the Scheme ID
            $qbu->setParameter('id', $type['id']);
            $qbu->setParameter('omekas_id', $scheme['o:id']);
            $qbu->setParameter('omekas_collection_id', $collection['o:id']);
            $qbu->setParameter('omekas_customvocab_id', $customvocab['o:id']);
            $qbu->setParameter('omekas_updated', $this->omekaSDateToTimestamp($scheme['o:modified']['@value']));
            $update = $qbu->execute();
            $this->connection()->commit();
            $this->connection()->beginTransaction();

            // Fetch and process all attrbutes
            $qbc->setParameter('attributetype', $settings['type']);
            $attributes = $qbc->execute()->fetchAllAssociative();
            foreach ($attributes as $attribute) {

                // Create/Update the SKOS Concept item
                $labels = $this->fetchAliases('cor_lut_attribute', $attribute['id']);
                $concept = $this->postSkosConcept(
                    $attribute['omekas_id'] ?? null,
                    $scheme['o:id'],
                    $collection['o:id'],
                    $labels,
                    $attribute['attribute'],
                    $settings['is_public'],
                    $settings['owner'],
                    $attribute['cre_on']
                );
                if (!$concept) {
                    $this->write('    FAILED CREATING VOCABULARY CONCEPT = ');
                    return false;
                }

                // Update scheme with inverse top properties
                $scheme['skos:hasTopConcept'][] = $this->makePropertyValue('skos:hasTopConcept', 'resource:item', $concept['o:id']);

                // Update the Atrribute record with the Concept ID
                $qba->setParameter('id', $attribute['id']);
                $qba->setParameter('omekas_id', $concept['o:id']);
                $qba->setParameter('omekas_updated', $this->omekaSDateToTimestamp($concept['o:modified']['@value']));
                $update = $qba->execute();

            }
            $this->connection()->commit();
            $this->connection()->beginTransaction();

            // Update the Scheme with the Concept inverses
            $inverses = [];
            $inverses['id'] = $scheme['o:id'];
            $inverses['values'] = $scheme;
            $scheme = $this->postResource($inverses);
            if (!$scheme) {
                $this->write('    FAILED UPDATING VOCABULARY SCHEME INVERSES');
                return false;
            }
        }

        return true;
    }

    // Export the Attribute Vocabularies to Omeka S
    protected function exportActionVocabularies() : bool
    {
        $this->write('');
        $this->write('Exporting Action Vocabularies');

        // Fetch Action Type
        $qbs = $this->connection()->createQueryBuilder();
        $qbs->select('typ.*')
            ->from('cor_lut_actiontype', 'typ')
            ->where('typ.actiontype = :actiontype');

        // Update Action Type
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update('cor_lut_actiontype', 'typ')
            ->where('typ.id = :id')
            ->set('typ.omekas_id', ':omekas_id')
            ->set('typ.omekas_scheme_id', ':omekas_scheme_id')
            ->set('typ.omekas_collection_id', ':omekas_collection_id')
            ->set('typ.omekas_customvocab_id', ':omekas_customvocab_id')
            ->set('typ.omekas_updated', ':omekas_updated');

        foreach ($this->vocabularies['action'] as $vocabulary => $settings) {
            $this->write('    Exporting '.$vocabulary);

            $this->connection()->beginTransaction();

            $collection = [];
            $customvocab = [];
            $scheme = [];
            $concept = [];
            foreach ($settings['type'] as $actiontype) {

                // Fetch the actiontype record
                $qbs->setParameter('actiontype', $actiontype);
                $type = $qbs->execute()->fetchAssociative();
                if (!$type) {
                    $this->write('    ERROR FETCHING ACTION TYPE');
                    return false;
                }

                // Create/Update the Collection Item Set for first actiontype record only
                if (!$collection) {
                    $collection = $this->postSkosCollection(
                        $type['omekas_collection_id'] ?? null,
                        $settings['collection'],
                        $settings['is_public'],
                        $settings['owner'],
                        $type['cre_on']
                    );
                    if (!$collection) {
                        $this->write('    FAILED CREATING VOCABULARY COLLECTION');
                        return false;
                    }
                    $collection_id = $collection['o:id'];
                }

                // Create/Update the Custom Vocab for first actiontype record only
                if (!$customvocab) {
                    $customvocab = $this->postCustomVocabulary(
                        $type['omekas_customvocab_id'] ?? null,
                        $settings['customvocab'],
                        $collection['o:id'],
                        $settings['is_public'],
                        $settings['owner']
                    );
                    if (!$customvocab) {
                        $this->write('    FAILED CREATING CUSTOM VOCABULARY');
                        return false;
                    }
                }

                // Create/Update the SKOS Scheme item for first actiontype record only
                if (!$scheme) {
                    $labels = ['en' => $settings['scheme']];
                    $scheme = $this->postSkosScheme(
                        $type['omekas_scheme_id'] ?? null,
                        $collection['o:id'],
                        $labels,
                        $settings['notation'],
                        $settings['is_public'],
                        $settings['owner'],
                        $type['cre_on']
                    );
                    if (!$scheme) {
                        $this->write('    FAILED CREATING VOCABULARY SCHEME');
                        return false;
                    }
                }

                // Create/Update the SKOS Concept item
                $labels = $this->fetchAliases('cor_lut_actiontype', $type['id']);
                $concept = $this->postSkosConcept(
                    $type['omekas_id'] ?? null,
                    $scheme['o:id'],
                    $collection['o:id'],
                    $labels,
                    $type['actiontype'],
                    $settings['is_public'],
                    $settings['owner'],
                    $type['cre_on']
                );
                if (!$concept) {
                    $this->write('    FAILED CREATING VOCABULARY CONCEPT = ');
                    return false;
                }

                // Update scheme with inverse top properties
                $scheme['skos:hasTopConcept'][] = $this->makePropertyValue('skos:hasTopConcept', 'resource:item', $concept['o:id']);

                // Update the Atrribute Type record with the SKOS IDs
                $qbu->setParameter('id', $type['id']);
                $qbu->setParameter('omekas_id', $concept['o:id']);
                $qbu->setParameter('omekas_scheme_id', $scheme['o:id']);
                $qbu->setParameter('omekas_collection_id', $collection['o:id']);
                $qbu->setParameter('omekas_customvocab_id', $customvocab['o:id']);
                $qbu->setParameter('omekas_updated', $this->omekaSDateToTimestamp($scheme['o:modified']['@value']));
                $update = $qbu->execute();
            }

            $this->connection()->commit();

            // Update the Scheme with the Concept inverses
            $inverses = [];
            $inverses['id'] = $scheme['o:id'];
            $inverses['values'] = $scheme;
            $scheme = $this->postResource($inverses);
            if (!$scheme) {
                $this->write('    FAILED UPDATING VOCABULARY SCHEME INVERSES');
                return false;
            }
        }

        return true;
    }

    // Export the Attribute Vocabularies to Omeka S
    protected function exportSiteVocabularies() : bool
    {
        $this->write('');
        $this->write('Exporting Site Vocabularies');

        // Fetch Site
        $qbs = $this->connection()->createQueryBuilder();
        $qbs->select('ste.*')
            ->from('cor_tbl_ste', 'ste')
            ->where('ste.id = :id');

        // Update Site
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update('cor_tbl_ste', 'ste')
            ->where('ste.id = :id')
            ->set('ste.omekas_id', ':omekas_id')
            ->set('ste.omekas_scheme_id', ':omekas_scheme_id')
            ->set('ste.omekas_collection_id', ':omekas_collection_id')
            ->set('ste.omekas_customvocab_id', ':omekas_customvocab_id')
            ->set('ste.omekas_updated', ':omekas_updated');

        foreach ($this->vocabularies['site'] as $vocabulary => $settings) {
            $this->write('    Exporting '.$vocabulary);

            $this->connection()->beginTransaction();

            $collection = [];
            $customvocab = [];
            $scheme = [];
            $concept = [];
            foreach ($settings['type'] as $ste_cd) {

                // Fetch the site record
                $qbs->setParameter('id', $ste_cd);
                $site = $qbs->execute()->fetchAssociative();
                if (!$site) {
                    $this->write('    ERROR FETCHING SITE');
                    return false;
                }

                // Create/Update the Collection Item Set for first site record only
                if (!$collection) {
                    $collection = $this->postSkosCollection(
                        $site['omekas_collection_id'] ?? null,
                        $settings['collection'],
                        $settings['is_public'],
                        $settings['owner'],
                        $site['cre_on']
                    );
                    if (!$collection) {
                        $this->write('    FAILED CREATING VOCABULARY COLLECTION');
                        return false;
                    }
                }

                // Create/Update the Custom Vocab for first site record only
                if (!$customvocab) {
                    $customvocab = $this->postCustomVocabulary(
                        $site['omekas_customvocab_id'] ?? null,
                        $settings['customvocab'],
                        $collection['o:id'],
                        $settings['is_public'],
                        $settings['owner']
                    );
                    if (!$customvocab) {
                        $this->write('    FAILED CREATING CUSTOM VOCABULARY');
                        return false;
                    }
                }

                // Create/Update the SKOS Scheme item for first site record only
                if (!$scheme) {
                    $labels = ['en' => $settings['scheme']];
                    $scheme = $this->postSkosScheme(
                        $site['omekas_scheme_id'] ?? null,
                        $collection['o:id'],
                        $labels,
                        $settings['notation'],
                        $settings['is_public'],
                        $settings['owner'],
                        $site['cre_on']
                    );
                    if (!$scheme) {
                        $this->write('    FAILED CREATING VOCABULARY SCHEME');
                        return false;
                    }
                }

                // Create/Update the SKOS Concept item
                $labels = ['en' => $ste_cd];
                $concept = $this->postSkosConcept(
                    $site['omekas_id'] ?? null,
                    $scheme['o:id'],
                    $collection['o:id'],
                    $labels,
                    $ste_cd,
                    $settings['is_public'],
                    $settings['owner'],
                    $site['cre_on']
                );
                if (!$concept) {
                    $this->write('    FAILED CREATING VOCABULARY CONCEPT = ');
                    return false;
                }

                // Update scheme with inverse top properties
                $scheme['skos:hasTopConcept'][] = $this->makePropertyValue('skos:hasTopConcept', 'resource:item', $concept['o:id']);

                // Update the Atrribute Type record with the SKOS IDs
                $qbu->setParameter('id', $site['id']);
                $qbu->setParameter('omekas_id', $concept['o:id']);
                $qbu->setParameter('omekas_scheme_id', $scheme['o:id']);
                $qbu->setParameter('omekas_collection_id', $collection['o:id']);
                $qbu->setParameter('omekas_customvocab_id', $customvocab['o:id']);
                $qbu->setParameter('omekas_updated', $this->omekaSDateToTimestamp($scheme['o:modified']['@value']));
                $update = $qbu->execute();
            }

            $this->connection()->commit();

            // Update the Scheme with the Concept inverses
            $inverses = [];
            $inverses['id'] = $scheme['o:id'];
            $inverses['values'] = $scheme;
            $scheme = $this->postResource($inverses);
            if (!$scheme) {
                $this->write('    FAILED UPDATING VOCABULARY SCHEME INVERSES');
                return false;
            }
        }

        return true;
    }

    // Create/Update a SKOS Collection
    protected function postSkosCollection($collection_id, $title, $is_public, $owner, $created) : array|false
    {
        $collection = [];
        $collection['api_resource'] = 'item_sets';
        if ($collection_id) {
            $collection['id'] = $collection_id;
        }
        $collection['values']['o:resource_class']['o:id'] = $this->omekas['resource_classes']['skos:Collection']['o:id'];
        $collection['values']['o:is_public'] = $is_public;
        $collection['values']['o:owner']['o:id'] = $owner;
        $collection['values']['o:created']['@value'] = $this->timestampToOmekaDate($created);
        $collection['values']['dcterms:title'][] = $this->makePropertyValue('dcterms:title', 'literal', $title);
        return $this->postResource($collection);
    }

    // Create/Update a Custom Vocabulary
    protected function postCustomVocabulary($customvocab_id, $label, $collection_id, $is_public, $owner) : array|false
    {
        $customvocab = [];
        $customvocab['api_resource'] = 'custom_vocabs';
        if ($customvocab_id) {
            $customvocab['id'] = $customvocab_id;
        }
        $customvocab['values']['o:label'] = $label;
        $customvocab['values']['o:owner']['o:id'] = $owner;
        $customvocab['values']['o:item_set']['o:id'] = $collection_id;
        return $this->postResource($customvocab);
    }

    // Create/Update a SKOS Scheme
    protected function postSkosScheme($scheme_id, $collection_id, array $labels, string $notation, $is_public, $owner, $created) : array|false
    {
        $template = $this->omekas['resource_templates']['Thesaurus Scheme'];
        $scheme = [];
        if ($scheme_id) {
            $scheme['id'] = $scheme_id;
        }
        $scheme['values']['o:resource_template']['o:id'] = $template['o:id'];
        $scheme['values']['o:resource_class']['o:id'] = $template['o:resource_class']['o:id'];
        $scheme['values']['o:is_public'] = $is_public;
        $scheme['values']['o:owner']['o:id'] = $owner;
        $scheme['values']['o:item_set'][] = ['o:id' => $collection_id];
        $scheme['values']['o:created']['@value'] = $this->timestampToOmekaDate($created);
        foreach ($labels as $language => $label) {
            $scheme['values']['skos:prefLabel'][] = $this->makePropertyValue('skos:prefLabel', 'literal', $label, $language);
        }
        $scheme['values']['skos:notation'][] = $this->makePropertyValue('skos:notation', 'literal', $notation);
        return $this->postResource($scheme);
    }

    // Create/Update a SKOS Concept
    protected function postSkosConcept($concept_id, $scheme_id, $collection_id, array $labels, string $notation, $is_public, $owner, $created) : array|false
    {
        $template = $this->omekas['resource_templates']['Thesaurus Concept'];
        $concept = [];
        if ($concept_id) {
            $concept['id'] = $concept_id;
        }
        $concept['values']['o:resource_template']['o:id'] = $template['o:id'];
        $concept['values']['o:resource_class']['o:id'] = $template['o:resource_class']['o:id'];
        $concept['values']['o:is_public'] = $is_public;
        $concept['values']['o:owner']['o:id'] = $owner;
        $concept['values']['o:item_set'][] = ['o:id' => $collection_id];
        $concept['values']['o:created']['@value'] = $this->timestampToOmekaDate($created);
        foreach ($labels as $language => $label) {
            $concept['values']['skos:prefLabel'][] = $this->makePropertyValue('skos:prefLabel', 'literal', $label, $language);
        }
        $concept['values']['skos:notation'][] = $this->makePropertyValue('skos:notation', 'literal', $notation);
        $concept['values']['skos:inScheme'][] = $this->makePropertyValue('skos:inScheme', 'resource:item', $scheme_id);
        $concept['values']['skos:topConceptOf'][] = $this->makePropertyValue('skos:topConceptOf', 'resource:item', $scheme_id);
        return $this->postResource($concept);
    }

    // Fetch all items for a module and map the values before exporting
    protected function exportModule(string $mod, array $item_cds = [], string $start = '', array $ste_cds = []) : bool
    {
        $this->write('');
        $this->write('Exporting module '.$mod);
        $count = 0;
        $created = 0;
        $updated = 0;

        $module = $this->modules[$mod];
        $mod_cd = $module['key']['mod_cd'];

        $this->connection()->beginTransaction();
        $qbi = $this->createItemsQueryBuilder($mod, $item_cds, $start, $ste_cds);
        if (!$item_cds && !$start && !$ste_cds && !$this->all) {
            $qbi->where('tbl.omekas_id IS NULL');
        }
        if (array_key_exists('status', $module['key'])) {
            $on = "stat.itemkey = '$mod_cd' AND stat.itemvalue = tbl.$mod_cd AND stat.attribute = :status";
            $qbi->leftJoin('tbl', 'cor_tbl_attribute', 'stat', $on);
            $qbi->andWhere('stat.boolean = 1');
            $qbi->setParameter('status', $module['key']['status']);
        }
        //$this->write($qbi->getSQL());
        //$this->writeArray($qbi->getParameters());

        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update($module['key']['tbl'], 'tbl')
            ->where($mod_cd.' = :id')
            ->set('tbl.omekas_id', ':omekas_id')
            ->set('tbl.omekas_updated', ':omekas_updated');

        $limit = 1000;
        $offset = 0;
        $count = 0;
        $qbi->setFirstResult($offset)->setMaxResults($limit);
        while ($items = $qbi->execute()->fetchAll()) {
            foreach ($items as $item) {
                $modtype = $item['type'] ?? $mod;

                // Map the ARK data to the Omeka ontology
                $this->processItem($mod, $modtype, $item);
                $this->processDatatype($mod, $modtype, 'txt', $item);
                $this->processDatatype($mod, $modtype, 'number', $item);
                $this->processDatatype($mod, $modtype, 'date', $item);
                $this->processDatatype($mod, $modtype, 'action', $item);
                $this->processDatatype($mod, $modtype, 'span', $item);
                $this->processDatatype($mod, $modtype, 'attribute', $item);
                $this->processSpatialDatatype($mod, $modtype, $item);
                $this->processDatatype($mod, $modtype, 'file', $item);
                $this->processXmi($mod, $modtype, $item);
                $this->processCustom($mod, $modtype, $item);

                // Post the new or updated Omeka Item
                $result = $this->postResource($item);
                if ($result === false) {
                    return false;
                }

                // If Post successful, update the ARK Item with result
                $qbu->setParameter('id', $item['item']);
                $qbu->setParameter('omekas_id', $result['o:id']);
                $qbu->setParameter('omekas_updated', $this->omekaSDateToTimestamp($result['o:modified']['@value']));
                $update = $qbu->execute();

                if (isset($item['id'])) {
                    $updated ++;
                } else {
                    $created ++;
                }

                $count ++;
                if ($count % 100 == 0) {
                    $this->connection()->commit();
                    $this->connection()->beginTransaction();
                    $this->write("    ... $count new = $created mod = $updated last item = ".$item['item']);
                }
            }
            $this->connection()->commit();
            $this->connection()->beginTransaction();
            unset($items);
            $offset += $limit;
            $qbi->setFirstResult($offset)->setMaxResults($limit);
        }
        $this->connection()->commit();
        $this->write("    Created $created Omeka records");
        $this->write("    Updated $updated Omeka records");

        return true;
    }

    // Process the Item
    protected function processItem(string $mod, string $modtype, array &$item) : void
    {
        $mapping = $this->modules[$mod]['modtype'][$modtype];
        $values = [];

        // Map the item resource
        $values['o:resource_template']['o:id'] = $mapping['resource_template_id'];
        $values['o:resource_class']['o:id'] = $mapping['resource_class_id'];
        $values['o:is_public'] = $mapping['is_public'];  // TODO: Get from record flag if update?
        $values['o:owner']['o:id'] = $mapping['owner'];
        $values['inverse_properties_set_inverses'] = 1;
        $values['o:created']['@value'] = $this->timestampToOmekaDate($item['created']);

        // Map the Item Sets
        foreach ($mapping['item_sets'] as $item_set_name) {
            $item_set_id = $this->omekas['item_sets'][$item_set_name]['o:id'];
            $values['o:item_set'][] = ['o:id' => $item_set_id];
        }

        // Map the Sites
        foreach ($mapping['sites'] as $site_name) {
            $site_id = $this->omekas['sites'][$site_name]['o:id'];
            $values['o:site'][] = ['o:id' => $site_id];
        }

        $item['values'] = $values;

        // Map the item key
        if ($this->modules[$mod]['core']) {
            $this->mapItemValue($mod, $modtype, 'cor', 'item', $item);
            $this->mapItemSite($mod, $modtype, 'cor', 'item_register', $item);
            $this->mapItemValue($mod, $modtype, 'cor', 'item_sequence', $item);
            // Map the Item type property (if exists)
            //$this->mapItemValue($mod, 'cor', 'type', $item);
        }
    }

    // Fetch all frags for a single datatype for Item as fastest way
    protected function processDatatype(string $mod, string $modtype, string $datatype, array &$item) : void
    {
        $type = $this->types[$datatype]['type'];
        $qbf = $this->connection()->createQueryBuilder();
        if ($datatype === 'attribute') {
            $qbf->select("tbl.*, users.username AS creator, lut.attributetype AS type, attr.attribute AS attribute, attr.omekas_id AS concept_id")
                ->from('cor_tbl_attribute', 'tbl')
                ->leftJoin('tbl', 'cor_lut_attribute', 'attr', "tbl.attribute = attr.id")
                ->leftJoin('tbl', 'cor_lut_attributetype', 'lut', "attr.attributetype = lut.id");
        } elseif ($datatype === 'file') {
            $qbf->select("tbl.*, file.filename, file.uri, lut.filetype AS type, file.omekas_id AS media_id, file.omekas_updated, users.username AS creator")
                ->from('cor_tbl_file', 'tbl')
                ->leftJoin('tbl', 'cor_lut_file', 'file', 'tbl.file = file.id')
                ->leftJoin('file', 'cor_lut_filetype', 'lut', 'file.filetype = lut.id');
        } elseif ($datatype === 'action') {
            $qbf->select("tbl.*, users.username AS creator, lut.$type AS type, abk.omekas_id AS actor_id, lut.omekas_id AS concept_id")
                ->from($this->types[$datatype]['tbl'], 'tbl')
                ->leftJoin('tbl', $this->types[$datatype]['lut'], 'lut', "tbl.$type = lut.id")
                ->leftJoin('tbl', 'abk_tbl_abk', 'abk', "abk.abk_cd = tbl.actor_itemvalue");
        } else {
            $qbf->select("tbl.*, users.username AS creator, lut.$type AS type")
                ->from($this->types[$datatype]['tbl'], 'tbl')
                ->leftJoin('tbl', $this->types[$datatype]['lut'], 'lut', "tbl.$type = lut.id");
        }
        $qbf->leftJoin('tbl', 'cor_tbl_users', 'users', 'tbl.cre_by = users.id')
            ->where('itemkey = :itemkey AND itemvalue = :itemvalue')
            ->orderBy('tbl.cre_on', 'ASC')
            ->setParameter('itemkey', $this->modules[$mod]['key']['mod_cd'])
            ->setParameter('itemvalue', $item['item']);
        if ($datatype === 'file') {
            $qbf->andWhere('file.omekas_id IS NOT NULL');
        }
        $frags = $qbf->execute()->fetchAllAssociative();
        $this->processFrags($mod, $modtype, $datatype, $item, $frags);
        unset($frags);
    }

    // Process all frags for Item and datatype
    protected function processFrags(string $mod, string $modtype, string $datatype, array &$item, array|null $frags) : void
    {
        foreach ($frags as $frag) {
            $type = $frag['type'];
            if ($type === null) {
                continue;
            }
            $mapping = $this->fieldMapping($mod, $modtype, $datatype, $type);
            if (!empty($mapping)) {
                $value = $mapping['value'];
                if ($datatype === 'date') {
                    // NOTE need custom formats due to PHP parsing MySQL partial dates incorrectly
                    if ($mapping['format'] === 'Y') {
                        // Storage format = YYYY-00-00, which PHP reads as 30 Nov prev year
                        $value['@value'] = substr($frag[$datatype], 0, 4);
                    } else {
                        $dt = ARK::utc($frag[$datatype]);
                        $value['@value'] = $dt->format($mapping['format']);
                    }
                } else if ($datatype === 'span') {
                    if ($mapping['spantype'] === 'daterange') {
                        $begin = $this->yearFormat($frag['beg']);
                        $end = $this->yearFormat($frag['end']);
                        $value['@value'] = $begin.'/'.$end;
                    } else {
                        $value['@value'] = $frag['beg'].'/'.$frag['end'];
                    }
                } else if ($datatype === 'attribute') {
                    $value['value_resource_id'] = $frag['concept_id'];
                } else if ($datatype === 'action') {
                    $value['value_resource_id'] = $frag['actor_id'];
                    if (array_key_exists('annotation', $mapping)) {
                        $property = $mapping['annotation']['property'];
                        $anno = $mapping['annotation']['value'];
                        $anno['value_resource_id'] = $frag['concept_id'];
                        $value['@annotation'][$property][] = $anno;
                    }
                } else if ($datatype === 'file') {
                    $value['value_resource_id'] = $frag['media_id'];
                    $item['values']['o:media'][] = ['o:id' => $frag['media_id']];
                } else if ($value['type'] === 'uri') {
                    $value['@id'] = (string) $frag[$datatype];
                } else {
                    $value['@value'] = (string) $frag[$datatype];
                }
                if ($mapping['language'] ?? false) {
                    $value['@language'] = $frag['language'];
                }
                if (!$mapping['multiple'] ) {
                    $item['values'][$mapping['property']] = [];
                }
                $item['values'][$mapping['property']][] = $value;
            }
        }
    }

    // Fetch all frags for a single datatype for Item as fastest way
    protected function processSpatialDatatype(string $mod, string $modtype, array &$item) : void
    {
        $spatial = $this->datatypeMapping($mod, $modtype, 'spatial');
        if (!$spatial) {
            return;
        }
        $mod_cd = $this->modules[$mod]['key']['mod_cd'];
        $type = $this->types['spatial']['type'];

        $qbf = $this->connection()->createQueryBuilder();
        $qbf->select("tbl.*, ST_AsWKT(geometry) AS geom_wkt, ST_AsWKT(bbox) AS bbox_wkt, users.username AS creator")
            ->from($this->types['spatial']['tbl'], 'tbl')
            ->leftJoin('tbl', 'cor_tbl_users', 'users', 'tbl.cre_by = users.id')
            ->where('itemkey = :itemkey AND itemvalue = :itemvalue')
            ->orderBy('tbl.cre_on', 'ASC')
            ->setParameter('itemkey', $mod_cd)
            ->setParameter('itemvalue', $item['item']);
        $frags = $qbf->execute()->fetchAllAssociative();

        foreach ($frags as $frag) {
            foreach ($spatial as $key => $mapping) {
                if ($frag[$type] === $mapping[$type]) {
                    if (!$this->projector) {
                        $this->projector = new Proj4Projector(4326);
                    }
                    $source = Geometry::fromText($frag[$mapping['datatype'].'_wkt'], $mapping['srid']);
                    // Filter all geometries in a collection are of the same type
                    $geometries = [];
                    if ($source->geometryType() === 'GeometryCollection') {
                        foreach ($source->geometries() as $geometry) {
                            if ($geometry->geometryType() === $mapping['geometry']) {
                                $geometries[] = $geometry;
                            } else if ($geometry->geometryType() === 'MultiPolygon' && $mapping['geometry'] === 'Polygon') {
                                foreach ($geometry->geometries() as $single) {
                                    $geometries[] = $single;
                                }
                            } else {
                                $this->write('DROPPED GEOMETRY FROM GEOMETRY COLLECTION '.$item['item'].' '.$key.' '.$geometry->geometryType());
                                //$this->write($frag[$mapping['datatype'].'_wkt']);
                            }
                        }
                    } else if ($source->geometryType() === 'MultiPolygon' && $mapping['geometry'] === 'Polygon') {
                        foreach ($source->geometries() as $geometry) {
                            $geometries[] = $geometry;
                        }
                    } else {
                        $geometries[] = $source;
                    }
                    if (!$mapping['multiple'] ) {
                        $item['values'][$mapping['property']] = [];
                    }
                    foreach ($geometries as $geometry) {
                        $value = $mapping['value'];
                        $target = $geometry->project($this->projector);
                        $value['@value'] = $target->asText();
                        $item['values'][$mapping['property']][] = $value;
                        if (!$mapping['multiple'] ) {
                            continue;
                        }
                    }
                }
            }
        }

        unset($frags);
    }

    // Process all XMI for Item
    protected function processXmi(string $mod, string $modtype, array &$item) : void
    {
        $xmimaps = $this->modules[$mod]['modtype'][$modtype]['datatype']['xmi'] ?? [];
        if (empty($xmimaps)) {
            return;
        }
        $mod_cd = $this->modules[$mod]['key']['mod_cd'];
        $qbx = $this->connection()->createQueryBuilder();
        $qbx->select("tbl.*, users.username AS creator")
            ->from('cor_tbl_xmi', 'tbl')
            ->leftJoin('tbl', 'cor_tbl_users', 'users', 'tbl.cre_by = users.id')
            ->where('(itemkey = :itemkey AND itemvalue = :itemvalue AND xmi_itemkey = :xmi_itemkey)
                     OR
                     (xmi_itemkey = :itemkey AND xmi_itemvalue = :itemvalue AND itemkey = :xmi_itemkey)')
            ->orderBy('tbl.cre_on', 'ASC')
            ->setParameter('itemkey', $mod_cd)
            ->setParameter('itemvalue', $item['item']);
        foreach ($xmimaps as $field => $mapping) {
            $xmi_mod = $mapping['module'];
            $xmi_itemkey = $this->modules[$xmi_mod]['key']['mod_cd'];
            $qbx->setParameter('xmi_itemkey', $xmi_itemkey);
            $qbi = $this->connection()->createQueryBuilder();
            $qbi->select('tbl.omekas_id')
                ->from($xmi_mod.'_tbl_'.$xmi_mod, 'tbl')
                ->where('tbl.'.$xmi_itemkey.' = :xmi_itemvalue');
            $xmis = $qbx->execute()->fetchAllAssociative();
            foreach ($xmis as $xmi) {
                if ($xmi['itemkey'] == $mod_cd && $xmi['itemvalue'] == $item['item']) {
                    $xmi_itemvalue = $xmi['xmi_itemvalue'];
                } else {
                    $xmi_itemvalue = $xmi['itemvalue'];
                }
                $qbi->setParameter('xmi_itemvalue', $xmi_itemvalue);
                $xmi_item = $qbi->execute()->fetchAssociative();
                if ($xmi_item) {
                    $value = $mapping['value'];
                    $value['value_resource_id'] = $xmi_item['omekas_id'];
                    if (!$mapping['multiple']) {
                        $item['values'][$mapping['property']] = [];
                    }
                    $item['values'][$mapping['property']][] = $value;
                }
            }
            unset($xmis);
        }
    }

    // Process any custom fields
    protected function processCustom(string $mod, string $modtype, array &$item) : void
    {
        $custom = $this->modules[$mod]['modtype'][$modtype]['datatype']['custom'] ?? [];
        foreach ($custom as $field => $mapping) {
            if (array_key_exists('method', $mapping)) {
                $method = $mapping['method'];
                $this->$method($item, $mod, $modtype, $field, $mapping);
            }
        }
    }

    // Custom process to map metadata based on related module type, i.e. Site based on Season type
    protected function processXmiMetadata(array &$item, string $mod, string $modtype, string $field, array $map) : void
    {
        $types = $this->fetchXmiTypes($mod, $item['item'], $map['module']);
        foreach ($types as $row) {
            $type = $row['type'];
            if (array_key_exists($type, $map['item_sets'])) {
                $item_set_name = $map['item_sets'][$type];
                $item_set_id = $this->omekas['item_sets'][$item_set_name]['o:id'];
                $item['values']['o:item_set'][] = ['o:id' => $item_set_id];
            }
            if (array_key_exists($type, $map['sites'])) {
                $site_name = $map['sites'][$type];
                $site_id = $this->omekas['sites'][$site_name]['o:id'];
                $item['values']['o:site'][] = ['o:id' => $site_id];
            }
        }
    }

    // Fetch all related XMI Items of a given Mod for a given Item
    protected function fetchXmiItems(string $mod, string $item, string $xmi_mod) : array
    {
        $mod_cd = $this->modules[$mod]['key']['mod_cd'];
        $xmi_module = $this->modules[$xmi_mod];
        $xmi_mod_cd = $xmi_module['key']['mod_cd'];
        $xmi_type = $xmi_module['key']['type'] ?? null;

        $xmi_items = [];
        $xmis = $this->fetchXmi($mod_cd, $item, $xmi_mod_cd);
        foreach ($xmis as $xmi) {
            if ($xmi['itemkey'] == $mod_cd && $xmi['itemvalue'] == $item) {
                $xmi_items[] = $xmi['xmi_itemvalue'];
            } else {
                $xmi_items[] = $xmi['itemvalue'];
            }
        }

        $qbi = $this->connection()->createQueryBuilder();
        if ($xmi_type) {
            $qbi->select("tbl.*, lut.$xmi_type AS type")
                ->from($xmi_module['key']['tbl'], 'tbl')
                ->leftJoin('tbl', $xmi_module['key']['lut'], 'lut', "tbl.$xmi_type = lut.id");
        } else {
            $qbi->select("tbl.*")
                ->from($xmi_module['key']['tbl'], 'tbl');
        }
        $qbi->where("tbl.$xmi_mod_cd IN (:xmi_itemvalue)")
            ->setParameter('xmi_itemvalue', $xmi_items, ArrayParameterType::STRING);

        return $qbi->execute()->fetchAllAssociative();
    }

    // Fetch all related XMI Items of a given Mod for a given Item
    protected function fetchXmiTypes(string $mod, string $item, string $xmi_mod) : array
    {
        $mod_cd = $this->modules[$mod]['key']['mod_cd'];
        $xmi_module = $this->modules[$xmi_mod];
        $xmi_mod_cd = $xmi_module['key']['mod_cd'];
        $xmi_type = $xmi_module['key']['type'];

        $xmi_items = [];
        $xmis = $this->fetchXmi($mod_cd, $item, $xmi_mod_cd);
        foreach ($xmis as $xmi) {
            if ($xmi['itemkey'] == $mod_cd && $xmi['itemvalue'] == $item) {
                $xmi_items[] = $xmi['xmi_itemvalue'];
            } else {
                $xmi_items[] = $xmi['itemvalue'];
            }
        }

        $qbi = $this->connection()->createQueryBuilder();
        $qbi->select("lut.$xmi_type AS type")
            ->distinct()
            ->from($xmi_module['key']['tbl'], 'tbl')
            ->leftJoin('tbl', $xmi_module['key']['lut'], 'lut', "tbl.$xmi_type = lut.id")
            ->where("tbl.$xmi_mod_cd IN (:xmi_itemvalue)")
            ->setParameter('xmi_itemvalue', $xmi_items, ArrayParameterType::STRING);

        return $qbi->execute()->fetchAllAssociative();
    }

    // Fetch all XMIs for a given Item and related Mod
    protected function fetchXmi(string $mod_cd, string $item, string $xmi_mod_cd) : array
    {
        $qbx = $this->connection()->createQueryBuilder();
        $qbx->select("xmi.*")
            ->from('cor_tbl_xmi', 'xmi')
            ->where('(itemkey = :itemkey AND itemvalue = :itemvalue AND xmi_itemkey = :xmi_itemkey)
                     OR
                     (xmi_itemkey = :itemkey AND xmi_itemvalue = :itemvalue AND itemkey = :xmi_itemkey)')
            ->orderBy('xmi.cre_on', 'ASC')
            ->setParameter('itemkey', $mod_cd)
            ->setParameter('itemvalue', $item)
            ->setParameter('xmi_itemkey', $xmi_mod_cd);

        return $qbx->execute()->fetchAllAssociative();
    }

    protected function mapGeonameValue(array &$item, array $geoname, string $mod, string $modtype, string $field) : void
    {
        $map = $this->fieldMapping($mod, $modtype, 'geonames', $field);
        $value = $map['value'];
        $value['@id'] = 'http://www.geonames.org/'.$geoname['geonameId'];
        $value['o:label'] = $geoname['name'];
        if (!$map['multiple']) {
            $item['values'][$map['property']] = [];
        }
        $item['values'][$map['property']][] = $value;
    }

    // Map the Omeka Mapping module fields
    protected function mapMappingPointValue(array &$item, $easting, $northing, string $label = null) : void
    {
        $mapping = [];
        $mapping['@type'] = 'o-module-mapping:Feature';
        $mapping['o-module-mapping:geography-type'] = 'Point';
        $mapping['o-module-mapping:geography-coordinates'] = [$easting, $northing];
        if ($label) {
            $mapping['o:label'] = $label;
        }
        $item['values']['o-module-mapping:feature'][] = $mapping;
    }

    // Export all Media items for a Module
    protected function exportMedia(string $mod, array $item_cds = [], string $start = '', array $ste_cds = []) : bool
    {
        $this->write('');
        $this->write('Exporting Media for Module '.$mod);
        $count = 0;

        $this->connection()->beginTransaction();

        // Get all items that are in Omeka, as Media must have an Item
        $qbi = $this->createItemsQueryBuilder($mod, $item_cds, $start, $ste_cds);
        $qbi->andWhere('tbl.omekas_id IS NOT NULL');

        $limit = 1000;
        $offset = 0;
        $count = 0;
        $media = 0;
        $qbi->setFirstResult($offset)->setMaxResults($limit);
        while ($items = $qbi->execute()->fetchAll()) {
            foreach ($items as $item) {
                $modtype = $item['type'] ?? $mod;


                // If file upload fails, then export fails
                $files = $this->uploadMediaFile($mod, $modtype, $item);
                if ($files === false) {
                    $this->write('Media Files Export Failed!');
                    return false;
                }
                $media += $files;

                // Custom Media Upload processing
                // If folder upload fails, then export fails
                $custom = $this->uploadMediaCustom($mod, $modtype, $item);
                if ($custom === false) {
                    $this->write('Custom Media File Export Failed!');
                    return false;
                }
                $media += $custom;

                // If no media uploaded then skip updating Omeka Item
                if ($files === 0 && $custom === 0) {
                    continue;
                }

                // Patch the Item with the media fields
                if (!$this->patchResource($item)) {
                    return false;
                }
                $count ++;
                if ($count % 100 == 0) {
                    $this->connection()->commit();
                    $this->connection()->beginTransaction();
                    $this->write('    ... '.$count.' Items '.$media.' Media');
                }
                //return;
            }
            $this->connection()->commit();
            $this->connection()->beginTransaction();
            unset($items);
            $offset += $limit;
            $qbi->setFirstResult($offset)->setMaxResults($limit);
        }
        $this->connection()->commit();
        $this->write("    Updated $count Omeka Items with $media Media");

        return true;
    }

    // Process all Files for Item
    protected function uploadMediaFile(string $mod, string $modtype, array &$item) : int|bool
    {
        $maps = $this->modules[$mod]['modtype'][$modtype]['datatype']['file'] ?? [];
        if (empty($maps)) {
            return 0;
        }
        $mapping = $this->modules[$mod]['modtype'][$modtype];
        $mod_cd = $this->modules[$mod]['key']['mod_cd'];
        $qbf = $this->connection()->createQueryBuilder();
        $qbf->select("tbl.*, file.filename, file.uri, lut.filetype AS type, file.omekas_id, file.omekas_updated, users.username AS creator")
            ->from('cor_tbl_file', 'tbl')
            ->leftJoin('tbl', 'cor_lut_file', 'file', 'tbl.file = file.id')
            ->leftJoin('file', 'cor_lut_filetype', 'lut', 'file.filetype = lut.id')
            ->leftJoin('tbl', 'cor_tbl_users', 'users', 'tbl.cre_by = users.id')
            ->where('tbl.itemkey = :itemkey AND tbl.itemvalue = :itemvalue AND file.omekas_id IS NULL')
            ->orderBy('tbl.cre_on', 'ASC')
            ->setParameter('itemkey', $mod_cd)
            ->setParameter('itemvalue', $item['item']);

        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update('cor_lut_file', 'tbl')
            ->where('id = :id')
            ->set('tbl.omekas_id', ':omekas_id')
            ->set('tbl.omekas_updated', ':omekas_updated');

        $count = 0;
        $frags = $qbf->execute()->fetchAllAssociative();
        foreach ($frags as $frag) {
            $field = $frag['type'];
            if (array_key_exists($field, $maps)) {
                $map = $maps[$field];

                $data = [];
                $files = [];
                if ($frag['omekas_id'] !== null) {
                    $data['o:id'] = $frag['omekas_id'];
                }
                $data['o:item']['o:id'] = $item['id'];
                $data['o:is_public'] = $mapping['is_public'];  // TODO: Get from record flag if update?
                $data['o:owner']['o:id'] = $mapping['owner'];
                if ($frag['uri'] !== null && str_contains($frag['uri'], 'youtube')) {
                    $data['o:ingester'] = 'youtube';
                    $data['o:source'] = $frag['uri'];
                } elseif ($frag['uri'] !== null) {
                    $data['o:ingester'] = 'url';
                    $data['o:source'] = $frag['uri'];
                } else {
                    $data['o:ingester'] = 'upload';
                    $data['o:source'] = $frag['filename'];
                    $data['file_index'] = 0;

                    // Extract the clean file extension from the filename
                    $parts = pathinfo($frag['filename']);
                    $extension = strtolower($parts['extension']);
                    switch ($extension) {
                        case 'jpeg':
                            $extension = 'jpg';
                            break;
                        case 'tiff':
                            $extension = 'tif';
                            break;
                    }

                    // Open the local file to upload
                    $path = $this->path.'/data/files/'.$frag['file'].'.'.$extension;
                    $handle = fopen($path, 'r');
                    if ($handle === false) {
                        $this->write('fopen failed for path '.$path);
                        return false;
                    }

                    // Standardise the filename with clean extension
                    $filename = $parts['filename'].'.'.$extension;
                    // Set the uploaded filename to the real filename
                    stream_context_set_option($handle, 'http', 'filename', $filename);
                    $files[] = $handle;
                }

                $result = $this->postMedia($data, $files);
                if ($data['o:ingester'] === 'upload') {
                    fclose($handle);
                }

                if ($result === false) {
                    return false;
                }

                // If Post successful, update the File Frag with result
                $qbu->setParameter('id', $frag['file']);
                $qbu->setParameter('omekas_id', $result['o:id']);
                $qbu->setParameter('omekas_updated', $this->omekaSDateToTimestamp($result['o:modified']['@value']));
                $update = $qbu->execute();

                $this->mapResourceValue($item, $map, $result['o:id']);

                $count ++;
            } else {
                $this->write('UNMAPPED FILE TYPE '.$field);
            }
        }

        return $count;
    }

    // Process any custom Media methods
    protected function uploadMediaCustom(string $mod, string $modtype, array &$item) : int|bool
    {
        $medias = 0;
        $maps = $this->modules[$mod]['modtype'][$modtype]['datatype']['media'] ?? [];
        foreach ($maps as $field => $map) {
            if (array_key_exists('method', $map)) {
                $method = $map['method'];
                $result = $this->$method($item, $mod, $modtype, $field, $map);
                if ($result === false) {
                    return false;
                }
                $medias += $result;
            }
        }
        return $medias;
    }

    // Post the Resource
    protected function postResource(array $resource) : array|bool
    {
        //$this->write('postResource');
        //$this->writeArray($resource);

        $api_resource = $resource['api_resource'] ?? 'items';

        if (isset($resource['id'])) {
            $response = $this->replaceOmekaSResource($api_resource, $resource['id'], $resource['values']);
        } else {
            $response = $this->createOmekaSResource($api_resource, $resource['values']);
        }

        if ($response->getStatusCode() == 200) {
            $result = $response->toArray();
            //$this->writeArray($result);
            return $result;
        }

        $this->write('POST FAILED!!!');
        $this->writeArray($resource);
        dump($response);

        return false;
    }

    // Patch the Resource
    protected function patchResource(array $resource) : array|bool
    {
        //$this->write('patchResource');
        //$this->writeArray($resource);

        $api_resource = $resource['api_resource'] ?? 'items';

        if (!isset($resource['id'])) {
            return false;
        }

        $response = $this->updateOmekaSResource($api_resource, $resource['id'], $resource['values']);

        if ($response->getStatusCode() == 200) {
            $result = $response->toArray();
            //$this->writeArray($result);
            return $result;
        }

        $this->write('PATCH FAILED!!!');
        $this->writeArray($resource);
        dump($response);

        return false;
    }

    // Process the Media
    protected function postMedia(array $data, array $files = []) : array|bool
    {
        //$this->write('postMedia');
        //$this->writeArray($data);

        // Assume for now only creating Media, not Updating.
        if (empty($files)) {
            $response = $this->createOmekaSResource('media', $data);
        } else {
            // $options['headers'] = [
            //     'Content-Type' => 'multipart/form-data',
            //     'Access-Control-Allow-Origin' => '*',
            // ];
            $options['query'] = $this->omekas['credentials'];
            $options['body']['data'] = json_encode($data);
            $options['body']['file'] = $files;
            //$this->writeArray($options);
            $response = $this->client->request('POST', $this->omekas['url'].'/api/media', $options);
        }

        if ($response->getStatusCode() == 200) {
            $result = $response->toArray();
            //$this->writeArray($result);
            return $result;
        }

        $this->write('POST FAILED!!!');
        $this->writeArray($options);
        dump($response);

        return false;
    }

    protected function yearFormat(string $source) : string
    {
        $year = 2000 - $source;
        return ($year < 0) ? sprintf('%05d', $year) : sprintf('%04d', $year);
    }

    protected function fieldMapping(string $mod, string $modtype, string $datatype, string $field) : array
    {
        if (isset($this->modules[$mod]['modtype'][$modtype]['datatype'][$datatype][$field])) {
            return $this->modules[$mod]['modtype'][$modtype]['datatype'][$datatype][$field];
        }
        return [];
    }

    protected function datatypeMapping(string $mod, string $modtype, string $datatype) : array
    {
        if (isset($this->modules[$mod]['modtype'][$modtype]['datatype'][$datatype])) {
            return $this->modules[$mod]['modtype'][$modtype]['datatype'][$datatype];
        }
        return [];
    }

    protected function mapItemSite(string $mod, string $modtype, string $datatype, string $field, array &$item) : void
    {
        if (isset($item[$field]) && isset($item['item_register_id'])) {
            $value = $item[$field];
            $mapping = $this->fieldMapping($mod, $modtype, $datatype, $field);
            $template = $mapping['value'];
            $template['value_resource_id'] = $item['item_register_id'];
            $item['values'][$mapping['property']][] = $template;
        }
    }

    protected function mapItemValue(string $mod, string $modtype, string $datatype, string $field, array &$item) : void
    {
        if (isset($item[$field])) {
            $value = $item[$field];
            $mapping = $this->fieldMapping($mod, $modtype, $datatype, $field);
            $template = $mapping['value'];
            if (isset($mapping['customvocab'])) {
                if (!array_key_exists($value, $mapping['values'])) {
                    return;
                }
                $concept = $mapping['values'][$value];
                $id = $this->omekas['custom_vocabs'][$mapping['customvocab']]['concepts'][$concept];
                $template['value_resource_id'] = $id;
            } else {
                $template['@value'] = $mapping['values'][$value] ?? $value;
            }
            $item['values'][$mapping['property']][] = $template;
        }
    }

    protected function mapResourceValue(array &$item, array $map, $id) : void
    {
        $value = $map['value'];
        $value['value_resource_id'] = $id;
        if (!$map['multiple']) {
            $item['values'][$map['property']] = [];
        }
        $item['values'][$map['property']][] = $value;
    }

    // Fetch required items for a module and map the values before exporting
    protected function createItemsQueryBuilder(string $mod, array $items = [], string $start = '', array $ste_cds = [])
    {
        $module = $this->modules[$mod]['key'];
        $mod_cd = $module['mod_cd'];
        $mod_no = $module['mod_no'];
        $modtype = $module['type'] ?? null;

        $this->connection()->beginTransaction();
        $qbi = $this->connection()->createQueryBuilder();
        $select = '';
        foreach ($this->modules[$mod]['select'] as $table => $fields) {
            foreach ($fields as $field => $alias) {
                $select .= "`$table`.`$field` AS `$alias`, ";
            }
        }
        $select .= "site.omekas_id AS item_register_id, users.username AS creator";
        $qbi->select($select)
            ->from($module['tbl'], 'tbl');
        $qbi->orderBy('tbl.ste_cd', 'ASC')
            ->addOrderBy("tbl.$mod_no", 'ASC');
        if ($modtype) {
            $qbi->leftJoin('tbl', $module['lut'], 'lut', "tbl.$modtype = lut.id");
        }
        $qbi->leftJoin('tbl', 'cor_tbl_ste', 'site', 'tbl.ste_cd = site.id');
        $qbi->leftJoin('tbl', 'cor_tbl_users', 'users', 'tbl.cre_by = users.id');
        if ($items) {
            $qbi->where("tbl.$mod_cd IN (:items)");
            $qbi->setParameter('items', $items, ArrayParameterType::STRING);
        } elseif ($start) {
            $parts = explode('_', $start);
            $qbi->where("tbl.ste_cd = :ste_cd AND $mod_no >= :mod_no");
            $qbi->setParameter('ste_cd', $parts[0]);
            $qbi->setParameter('mod_no', (int) $parts[1]);
        } elseif ($ste_cds) {
            $qbi->where('tbl.ste_cd IN (:ste_cds)');
            $qbi->setParameter('ste_cds', $ste_cds, ArrayParameterType::STRING);
        }

        return $qbi;
    }

    // Fetch all frags for a given Item, Datatype, and Fields
    protected function fetchAllFragsForTypes(string $mod, string $item, string $datatype, array $fields = [])
    {
        $module = $this->modules[$mod];
        $mod_cd = $module['key']['mod_cd'];
        $datatype = $this->types[$datatype];
        $type = $datatype['type'];

        $qbf = $this->connection()->createQueryBuilder();
        $qbf->select("tbl.*, lut.$type AS type")
            ->from($datatype['tbl'], 'tbl')
            ->leftJoin('tbl', $datatype['lut'], 'lut', "tbl.$type = lut.id")
            ->where("itemkey = :itemkey AND itemvalue = :itemvalue AND lut.$type IN (:types)")
            ->orderBy('tbl.cre_on', 'ASC')
            ->setParameter('itemkey', $mod_cd)
            ->setParameter('itemvalue', $item)
            ->setParameter('types', $fields, ArrayParameterType::STRING);
        return $qbf->execute()->fetchAllAssociative();
    }

    // Fetch all aliases
    protected function fetchAliases(string $itemkey, string $itemvalue, int $aliastype = 1) : array
    {
        // Fetch Labels
        $qbl = $this->connection()->createQueryBuilder();
        $qbl->select('alias.*')
            ->from('cor_tbl_alias', 'alias')
            ->where('alias.aliastype = :aliastype AND alias.itemkey = :itemkey AND alias.itemvalue = :itemvalue')
            ->orderBy('language', 'ASC');
        $qbl->setParameter('aliastype', $aliastype);
        $qbl->setParameter('itemkey', $itemkey);
        $qbl->setParameter('itemvalue', $itemvalue);
        $aliases = $qbl->execute()->fetchAllAssociative();
        // Sort result into keyed array with English first
        $result = [];
        foreach ($aliases as $alias) {
            if ($alias['language'] === 'en') {
                $result['en'] = $alias['alias'];
            }
        }
        foreach ($aliases as $alias) {
            if ($alias['language'] !== 'en') {
                $result[$alias['language']] = $alias['alias'];
            }
        }
        return $result;
    }

    protected function writeArray(array $value) : void
    {
        $this->write(print_r($value, true));
    }

    protected function getOmekaSResources(string $resource, array $query = [])
    {
        return $this->getOmekaS('/api/'.$resource, $query);
    }

    protected function getOmekaSResource(string $resource, $id, array $query = [])
    {
        return $this->getOmekaS('/api/'.$resource.'/'.$id, $query);
    }

    protected function createOmekaSResource(string $resource, array $json, array $query = [])
    {
        return $this->postOmekaS('/api/'.$resource, $json, $query);
    }

    protected function replaceOmekaSResource(string $resource, $id, array $json, array $query = [])
    {
        return $this->putOmekaS('/api/'.$resource.'/'.$id, $json, $query);
    }

    protected function updateOmekaSResource(string $resource, $id, array $json, array $query = [])
    {
        // The Omeka S PATCH only patches 'core' fields, if any Properties are passed then it is effectively a PUT
        // We work around this by first doing a GET for the resource and applying the patch ourselves
        $response = $this->getOmekaS('/api/'.$resource.'/'.$id, $query);
        if ($response->getStatusCode() !== 200) {
            return $result;
        }
        $item = $response->toArray();
        unset($item['@reverse']);
        foreach ($json as $key => $val) {
            $item[$key] = $val;
        }
        return $this->patchOmekaS('/api/'.$resource.'/'.$id, $item, $query);
    }

    // Get from API
    protected function getOmekaS(string $path, array $query = [])
    {
        $options['query'] =  array_merge($query, $this->omekas['credentials']);
        return $this->client->request('GET', $this->omekas['url'].$path, $options);
    }

    // Post new resource
    protected function postOmekaS(string $path, array $json, array $query = [])
    {
        $options['query'] =  array_merge($query, $this->omekas['credentials']);
        $options['json'] = $json;
        return $this->client->request('POST', $this->omekas['url'].$path, $options);
    }

    // Put full existing resource
    protected function putOmekaS(string $path, array $json, array $query = [])
    {
        $options['query'] =  array_merge($query, $this->omekas['credentials']);
        $options['json'] = $json;
        return $this->client->request('PUT', $this->omekas['url'].$path, $options);
    }

    // Patch partial existing resource
    protected function patchOmekaS(string $path, array $json, array $query = [])
    {
        $options['query'] =  array_merge($query, $this->omekas['credentials']);
        $options['json'] = $json;
        return $this->client->request('PATCH', $this->omekas['url'].$path, $options);
    }

    protected function omekaSDateToTimestamp(string $input)
    {
        return str_replace('T', ' ', substr($input, 0, 19));
    }

    protected function timestampToOmekaDate(string $input)
    {
        return str_replace(' ', 'T', substr($input, 0, 19));
    }

    // Get from API
    protected function getGeonames(string $path, array $query = [])
    {
        try {
            $options['query'] =  array_merge($query, $this->geonames['credentials']);
            $result = $this->client->request('GET', $this->geonames['url'].$path, $options);
            return $result;
        } catch (Throwable $e) {
            return [];
        }
    }
}
