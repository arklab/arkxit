<?php

/**
 * ARK Console Command.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace App\Console\Command;

use ARK\ARK;
use ARK\DBAL\Console\Command\DatabaseCommand;
use Symfony\Component\Filesystem\Filesystem;

class ArkExportTranslationCommand extends DatabaseCommand
{
    private $language = 'en';
    private $markupPrefix = 'ark.markup.';
    private $aliasPrefix = 'ark.alias.';
    private $path = '';
    private $properties = [];

    protected function configure() : void
    {
        $this->setName('ark:export:translation')
            ->setDescription('Export an ARK 1 translations');
    }

    protected function doExecute() : int
    {
        $this->path = ARK::installDir().'/export/'.$this->connection()->getDatabase().'/';
        if (!is_dir($this->path)) {
            $fs = new Filesystem();
            $fs->mkdir($this->path);
        }

        $this->connection()->beginTransaction();

        $this->write('Export Transactions');
        $languages = $this->languages();
        $this->write(implode(', ', $languages));
        $translations = $this->markup();
        $sources = $this->aliasSources();
        foreach ($sources as $source) {
            $translations = array_merge($translations, $this->alias($source));
        }
        $this->exportTransifexCsv($languages, $translations);

        return 0;
    }

    private function languages() : array
    {
        $qry = $this->connection()->createQueryBuilder();
        $qry->select('DISTINCT(language)')
            ->from('cor_tbl_markup', 'tbl')
            ->where('tbl.language != :default')
            ->setParameter('default', $this->language);
        $markup = $qry->execute()->fetchFirstColumn();

        $qry = $this->connection()->createQueryBuilder();
        $qry->select('DISTINCT(language)')
            ->from('cor_tbl_alias', 'tbl')
            ->where('tbl.language != :default')
            ->setParameter('default', $this->language);
        $alias = $qry->execute()->fetchFirstColumn();

        $langs = array_unique(array_merge($markup, $alias));
        sort($langs);
        array_unshift($langs, $this->language);
        return $langs;
    }

    private function markup() : array
    {
        $this->write('Fetching Markup');
        $markup = [];

        $limit = 1000;
        $offset = 0;
        $count = 0;
        $qry = $this->connection()->createQueryBuilder();
        $qry->select('tbl.*')
            ->from('cor_tbl_markup', 'tbl')
            ->orderBy('nname', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        while ($rows = $qry->execute()->fetchAllAssociative()) {
            foreach ($rows as $row) {
                $keyword = $this->markupPrefix.$row['nname'];
                $language = $row['language'];
                $trans = [];
                $trans['message'] = mb_trim($row['markup']);
                $trans['notes'] = mb_trim($row['description']);
                if (!in_array($keyword, array_keys($markup))) {
                    $markup[$keyword] = [];
                }
                $markup[$keyword][$language] = $trans;
                $count ++;
            }
            $this->write('    '.$count.'...');
            $offset += $limit;
            $qry->setFirstResult($offset)->setMaxResults($limit);
        }

        return $markup;
    }

    private function aliasSources() : array
    {
        $qry = $this->connection()->createQueryBuilder();
        $qry->select('DISTINCT(itemkey)')
            ->from('cor_tbl_alias', 'tbl')
            ->orderBy('itemkey', 'ASC');
        $source = $qry->execute()->fetchFirstColumn();

        sort($source);
        return $source;
    }

    private function alias(string $source) : array
    {
        $this->write('Fetching Aliases for '.$source);
        $parts = explode('_', $source);
        $module = $parts[0];
        $type = $parts[2];
        if ($source === 'cor_tbl_col') {
            $sourceType = 'dbname';
        } elseif ($source === 'cor_tbl_module') {
            $sourceType = 'name';
        } else {
            $sourceType = $type;
        }

        $alias = [];

        $limit = 1000;
        $offset = 0;
        $count = 0;
        $qry = $this->connection()->createQueryBuilder();
        $qry->select("source.$sourceType AS source", 'typ.aliastype', 'alias.language', 'alias.alias')
            ->from('cor_tbl_alias', 'alias')
            ->leftJoin('alias', 'cor_lut_aliastype', 'typ', 'typ.id = alias.aliastype')
            ->innerJoin('alias', $source, 'source', 'source.id = alias.itemvalue')
            ->where('alias.itemkey = :itemkey')
            ->setParameter('itemkey', $source)
            ->orderBy("source.$sourceType", 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        while ($rows = $qry->execute()->fetchAllAssociative()) {
            foreach ($rows as $row) {
                $keyword = $this->aliasPrefix.$type.'.'.$row['source'];
                if ($row['aliastype'] !== 'normal') {
                    $keyword = $keyword.'.'.$row['aliastype'];
                }
                $language = $row['language'];
                $trans = [];
                $trans['message'] = mb_trim($row['alias']);
                $trans['notes'] = '';
                if (!in_array($keyword, array_keys($alias))) {
                    $alias[$keyword] = [];
                }
                $alias[$keyword][$language] = $trans;
                $count ++;
            }
            $this->write('    '.$count.'...');
            $offset += $limit;
            $qry->setFirstResult($offset)->setMaxResults($limit);
        }

        return $alias;
    }

    private function exportTransifexCsv(array $languages, array $translations) : void
    {
        $this->write('Exporting to Transifex CSV');

        $path = $this->path.'/transifex.csv';
        $fp = fopen($path, 'w');

        $header = [
            'term',
            'pos',
            'notes',
            'is_case_sensitive',
        ];
        foreach ($languages as $language) {
            $header[] = 'translation_'.$language;
            $header[] = 'notes_'.$language;
        }
        fputcsv($fp, $header);

        foreach ($translations as $keyword => $translation) {
            $row = [];
            $row['term'] = $keyword;
            $row['pos'] = '';
            $row['notes'] = '';
            $row['is_case_sensitive'] = '';
            foreach ($languages as $language) {
                $default = $translation[$this->language]['message'] ?? '';
                $message = $translation[$language]['message'] ?? '';
                if ($language !== $this->language && $message === $default) {
                    $message = '';
                }
                $row['translation_'.$language] = $message;
                $notes = $translation[$language]['notes'] ?? '';
                if ($language === $this->language) {
                    $row['notes'] = $notes;
                }
                if ($notes === $row['notes'] || $notes === $default) {
                    $notes = '';
                }
                $row['notes_'.$language] = $notes;
            }
            fputcsv($fp, $row);
        }
    }
}
