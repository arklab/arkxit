<?php

/**
 * ARK Console Command.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace App\Console\Command;

use ARK\ARK;
use ARK\DBAL\Console\Command\DatabaseCommand;
use Symfony\Component\Filesystem\Filesystem;

class ArkExportCsvCommand extends DatabaseCommand
{
    private $language = 'en';
    private $path = '';
    private $properties = [];

    protected function configure() : void
    {
        $this->setName('ark:export:csv')
            ->setDescription('Export an ARK 1 site tables in flat CSV format');
    }

    protected function doExecute() : int
    {
        $this->path = ARK::installDir().'/export/'.$this->connection()->getDatabase().'/';
        if (!is_dir($this->path)) {
            $fs = new Filesystem();
            $fs->mkdir($this->path);
        }

        $this->connection()->beginTransaction();
        $this->export();
        return 0;
    }

    protected function export() : void
    {
        // USERS
        $this->write('Exporting Users...');
        $header = [
            'user_id',
            'username',
            'firstname',
            'lastname',
            'initials',
            'actor_code',
        ];
        if ($this->connection()->table('cor_tbl_users')->hasColumn('itemvalue')) {
            $select = [
                'user.id as user_id',
                'user.username',
                'user.firstname',
                'user.lastname',
                'user.initials',
                'user.itemvalue AS actor_code',
            ];
        } else {
            $select = [
                'user.id as user_id',
                'user.username',
                'user.firstname',
                'user.lastname',
                'user.initials',
                'null AS actor_code',
            ];
        }
        $qry = $this->connection()->createQueryBuilder();
        $qry->select($select)
            ->from('cor_tbl_users', 'user');
        $this->exportQuery($qry, 'user', $header);

        // SITES
        $this->write('Exporting Sites...');
        $header = [
            'site_code',
            'site_name',
            'created_by',
            'created_on',
        ];
        $select = [
            'site.id AS site_code',
            'site.description AS site_name',
            "COALESCE(user.username, 'unknown') AS created_by",
            'site.cre_on AS created_on',
        ];
        $qry = $this->connection()->createQueryBuilder();
        $qry->select($select)
            ->from('cor_tbl_ste', 'site')
            ->leftJoin('site', 'cor_tbl_users', 'user', 'site.cre_by = user.id');
        $this->exportQuery($qry, 'site', $header);

        // MODULES
        $this->write('Exporting Modules...');
        $header = [
            'module_code',
            'description',
            'created_by',
            'created_on',
        ];
        $select = [
            "CASE WHEN module.shortform = 'abk' THEN 'actor' ELSE module.shortform END AS module_code",
            'module.description',
            "COALESCE(user.username, 'unknown') AS created_by",
            'module.cre_on AS created_on'
        ];
        $qry = $this->connection()->createQueryBuilder();
        $qry->select($select)
            ->from('cor_tbl_module', 'module')
            ->leftJoin('module', 'cor_tbl_users', 'user', 'module.cre_by = user.id')
            ->where("module.shortform != 'cor'");
        $this->exportQuery($qry, 'module', $header);

        // PROPERTIES
        $this->write('Exporting Module Properties...');
        $this->datatypeProperties('action');
        $this->datatypeProperties('attribute');
        $this->datatypeProperties('date');
        $this->datatypeProperties('file');
        $this->datatypeProperties('number');
        $this->datatypeProperties('span');
        $this->datatypeProperties('txt');
        $this->xmiProperties();
        $this->exportProperties();

        // ATTRIBUTES
        $this->write('Exporting Attributes...');
        $this->exportAttributes();

        // ITEMS
        $this->write('Exporting Items...');
        $modules = $this->connection()->fetchAllTable('cor_tbl_module');
        $mod_codes = [];
        $itemkeys = [];
        foreach ($modules as $module) {
            $mod = $module['shortform'];
            if ($mod === 'cor') {
                continue;
            }
            $mod_codes[$module['itemkey']] = $mod;
            $itemkeys[] = $module['itemkey'];
            $this->write('  Exporting '.$mod);
            $tbl = $mod.'_tbl_'.$mod;
            $modtype = $mod.'type';
            $lut = $mod.'_lut_'.$modtype;
            $mod_cd = $mod.'_cd';
            $mod_no = $mod.'_no';
            if ($mod === 'abk') {
                $mod = 'actor';
            }
            if (!$this->connection()->tableExists($lut)) {
                $modtype = null;
                $lut = null;
            }
            $header = [
                'module_code',
                'item_code',
                'site_code',
                'site_index',
                'item_type',
                'created_by',
                'created_on',
            ];
            if ($modtype) {
                $select = [
                    "'$mod' AS module_code",
                    "item.$mod_cd AS item_code",
                    'item.ste_cd AS site_code',
                    "item.$mod_no AS site_index",
                    "lut.$modtype AS item_type",
                    "COALESCE(user.username, 'unknown') AS created_by",
                    'item.cre_on AS created_on',
                ];
            } else {
                $select = [
                    "'$mod' AS module_code",
                    "item.$mod_cd AS item_code",
                    'item.ste_cd AS site_code',
                    "item.$mod_no AS site_index",
                    "null AS item_type",
                    "COALESCE(user.username, 'unknown') AS created_by",
                    'item.cre_on AS created_on',
                ];
            }
            $qry = $this->connection()->createQueryBuilder();
            $qry->select($select)
                ->from($tbl, 'item')
                ->leftJoin('item', 'cor_tbl_users', 'user', 'item.cre_by = user.id');
            if ($modtype) {
                $qry->leftJoin('item', $lut, 'lut', "item.$modtype = lut.id");
            }
            $qry->orderBy('item.ste_cd', 'ASC')
                ->addOrderBy("item.$mod_no", 'ASC');
            $this->exportQuery($qry, 'item_'.$mod, $header);

            if ($modtype) {
                // Output the list of actual module type values used
                $header = [
                    'module_code',
                    'item_type',
                    'label',
                ];
                $qry = $this->connection()->createQueryBuilder();
                $qry->select("'$mod' AS module_code", "lut.$modtype AS item_type", 'alias.alias AS label')
                    ->from($lut, 'lut')
                    ->leftJoin('lut', 'cor_tbl_alias', 'alias', "alias.itemkey = '$lut' AND alias.itemvalue = lut.id AND alias.language = 'en'")
                    ->orderBy('item_type', 'ASC');
                $this->exportQuery($qry, 'module_'.$mod.'_item', $header);
            }
        }

        // ACTIONS
        $this->write('Exporting Actions...');
        $header = [
            'actor_code',
        ];
        $select = [
            'tbl.actor_itemvalue AS actor_code',
        ];
        $this->exportDatatype('action', $select, $header);

        // ATTRIBUTES
        $this->write('Exporting Attributes...');
        $header = [
            'attribute',
        ];
        $select = [
            'attribute.attribute AS attribute',
        ];
        $this->exportDatatype('attribute', $select, $header);

        // DATE
        $this->write('Exporting Dates...');
        $header = [
            'date',
        ];
        $select = [
            'tbl.date',
        ];
        $this->exportDatatype('date', $select, $header);

        // FILES
        $this->write('Exporting Files...');
        $header = [
            'filename',
            'uri',
            'file_id',
            'storage_filename',
        ];
        $select = [
            'file.filename',
            'file.uri',
            'file.id AS file_id',
            "CONCAT(file.id, '.', substring_index(file.filename, '.', -1)) AS storage_filename",
        ];
        $this->exportDatatype('file', $select, $header);

        // NUMBER
        $this->write('Exporting Numbers...');
        $header = [
            'number',
        ];
        $select = [
            'tbl.number',
        ];
        $this->exportDatatype('number', $select, $header);

        // SPAN
        $this->write('Exporting Spans...');
        $header = [
            'span_start',
            'span_end',
        ];
        $select = [
            'tbl.beg AS span_start',
            'tbl.end AS span_end',
        ];
        $this->exportDatatype('span', $select, $header);

        // TEXT
        $this->write('Exporting Text...');
        $header = [
            'text',
            'language',
        ];
        $select = [
            'tbl.txt AS text',
            'tbl.language',
        ];
        $this->exportDatatype('txt', $select, $header);

        // XMI
        $this->write('Exporting XMIs...');
        $header = [
            'module_code',
            'item_code',
            'site_code',
            'site_index',
            'datatype',
            'related_module_code',
            'related_item_code',
            'related_site_code',
            'related_site_index',
            'created_by',
            'created_on',
        ];
        $select = [
            "CASE WHEN module.shortform = 'abk' THEN 'actor' ELSE module.shortform END AS module_code",
            'tbl.itemvalue AS item_code',
            "substring_index(tbl.itemvalue, '_', 1) AS site_code",
            "substring_index(tbl.itemvalue, '_', -1) AS site_index",
            "'relation' AS datatype",
            "CASE WHEN xmi.shortform = 'abk' THEN 'actor' ELSE xmi.shortform END AS related_module_code",
            'tbl.itemvalue AS related_item_code',
            "substring_index(tbl.itemvalue, '_', 1) AS related_site_code",
            "substring_index(tbl.itemvalue, '_', -1) AS related_site_index",
            "COALESCE(user.username, 'unknown') AS created_by",
            'tbl.cre_on AS created_on',
        ];
        $qry = $this->connection()->createQueryBuilder();
        $qry->select($select)
            ->from('cor_tbl_xmi', 'tbl')
            ->leftJoin('tbl', 'cor_tbl_module', 'module', 'tbl.itemkey = module.itemkey')
            ->leftJoin('tbl', 'cor_tbl_module', 'xmi', 'tbl.xmi_itemkey = xmi.itemkey')
            ->leftJoin('tbl', 'cor_tbl_users', 'user', 'tbl.cre_by = user.id')
            ->orderBy('module_code')
            ->addOrderBy('site_code')
            ->addOrderBy('site_index')
            ->addOrderBy('related_module_code')
            ->addOrderBy('related_site_code')
            ->addOrderBy('related_site_index');
        $this->exportQuery($qry, "datatype_relation", $header);
    }

    private function exportDatatype(string $datatype, array $select, array $header)
    {
        $type = $datatype.'type';
        $tbl = "cor_tbl_$datatype";
        $ext = "cor_lut_$datatype";
        $lut = "cor_lut_$type";

        $qry = $this->connection()->createQueryBuilder();
        $qry->select('COUNT(*)')
            ->from('cor_tbl_action', 'tbl')
            ->where("tbl.itemkey = '$tbl'");
        $action_chains = (int) $qry->execute()->fetchOne();

        $qry = $this->connection()->createQueryBuilder();
        $qry->select('COUNT(*)')
            ->from('cor_tbl_date', 'tbl')
            ->where("tbl.itemkey = '$tbl'");
        $date_chains = (int) $qry->execute()->fetchOne();

        $chains = ($action_chains > 0 or $date_chains > 0);

        $core_header = [
            'module_code',
            'item_code',
            'site_code',
            'site_index',
            'datatype',
            'property',
        ];
        $core = [
            "CASE WHEN module.shortform = 'abk' THEN 'actor' ELSE module.shortform END AS module_code",
            'tbl.itemvalue AS item_code',
            "substring_index(tbl.itemvalue, '_', 1) AS site_code",
            "substring_index(tbl.itemvalue, '_', -1) AS site_index",
            "'$datatype' AS datatype",
            "lut.$type AS property",
        ];
        $action_header = [
            'action_type',
            'action_actor_code',
            'action_date_type',
            'action_date',
        ];
        if ($chains) {
            $action = [
                'actiontype.actiontype AS action_type',
                'action.actor_itemvalue AS action_actor_code',
                'datetype.datetype AS action_date_type',
                'date.date AS action_date',
            ];
        } else {
            $action = [
                'null AS action_type',
                'null AS action_actor_code',
                'null AS action_date_type',
                'null AS action_date',
            ];
        }
        $created_header = [
            'created_by',
            'created_on',
        ];
        $created = [
            "COALESCE(user.username, 'unknown') AS created_by",
            'tbl.cre_on AS created_on',
        ];
        $sel = array_merge($core, $select, $action, $created);
        $hdr = array_merge($core_header, $header, $action_header, $created_header);
        $qry = $this->connection()->createQueryBuilder();
        $qry->select($sel)
            ->from($tbl, 'tbl')
            ->innerJoin('tbl', 'cor_tbl_module', 'module', 'tbl.itemkey = module.itemkey');

        if ($this->connection()->tableExists($ext)) {
            $qry->leftJoin('tbl', $ext, $datatype, "tbl.$datatype = $datatype.id")
                ->leftJoin($datatype, $lut, 'lut', "$datatype.$type = lut.id")
                ->leftJoin('tbl', 'cor_tbl_users', 'user', 'tbl.cre_by = user.id');
        } else {
            $qry->leftJoin('tbl', $lut, 'lut', "tbl.$type = lut.id")
                ->leftJoin('tbl', 'cor_tbl_users', 'user', 'tbl.cre_by = user.id');
        }

        $qry->orderBy('module_code')
            ->addOrderBy('site_code')
            ->addOrderBy('site_index')
            ->addOrderBy('property');
        if ($chains) {
            $qry->leftJoin('tbl', 'cor_tbl_action', 'action', "action.itemkey = '$tbl' AND action.itemvalue = tbl.id")
                ->leftJoin('action', 'cor_lut_actiontype', 'actiontype', 'action.actiontype = actiontype.id')
                ->leftJoin('tbl', 'cor_tbl_date', 'date', "date.itemkey = '$tbl' AND date.itemvalue = tbl.id")
                ->leftJoin('date', 'cor_lut_datetype', 'datetype', "date.datetype = datetype.id");
        }
        $this->exportQuery($qry, "property_$datatype", $hdr);
    }

    private function exportQuery($qb, $filename, $header = []) : void
    {
        $limit = 1000;
        $offset = 0;
        $count = 0;
        $qb->setFirstResult($offset)->setMaxResults($limit);
        $path = $this->path.'/'.$filename.'.csv';
        $fp = fopen($path, 'w');
        if ($header) {
            fputcsv($fp, $header);
        }
        while ($rows = $qb->execute()->fetchAllAssociative()) {
            foreach ($rows as $row) {
                fputcsv($fp, $row);
                $count ++;
            }
            $this->write('    '.$count.'...');
            $offset += $limit;
            $qb->setFirstResult($offset)->setMaxResults($limit);
        }
        fclose($fp);
    }

    private function datatypeProperties(string $datatype) : void
    {
        $this->write('    '.$datatype);
        $type = $datatype.'type';
        $tbl = "cor_tbl_$datatype";
        $ext = "cor_lut_$datatype";
        $lut = "cor_lut_$type";

        $sel = [
            "CASE WHEN module.shortform = 'abk' THEN 'actor' ELSE module.shortform END AS module_code",
            "lut.$type AS property",
            "'$datatype' as datatype",
            'alias.alias AS property_label',
        ];

        $qry = $this->connection()->createQueryBuilder();
        $qry->select($sel)
            ->distinct()
            ->from($tbl, 'tbl');

        if ($this->connection()->tableExists($ext)) {
            $qry->leftJoin('tbl', $ext, $datatype, "tbl.$datatype = $datatype.id")
                ->leftJoin($datatype, $lut, 'lut', "$datatype.$type = lut.id");
        } else {
            $qry->leftJoin('tbl', $lut, 'lut', "tbl.$type = lut.id");
        }

        $qry->innerJoin('tbl', 'cor_tbl_module', 'module', 'tbl.itemkey = module.itemkey')
            ->leftJoin('lut', 'cor_tbl_alias', 'alias', "alias.itemkey = '$lut' AND alias.itemvalue = lut.id AND alias.language = '$this->language'")
            ->orderBy('module.shortform', 'ASC')
            ->addOrderBy("lut.$type", 'ASC');

        $limit = 1000;
        $offset = 0;
        $qry->setFirstResult($offset)->setMaxResults($limit);
        while ($rows = $qry->execute()->fetchAllAssociative()) {
            foreach ($rows as $row) {
                $this->properties[$row['module_code']][$row['property']] = $row;
            }
            $offset += $limit;
            $qry->setFirstResult($offset)->setMaxResults($limit);
        }
    }

    private function xmiProperties() : void
    {
        $this->write('    xmi');
        $select = [
            "CASE WHEN module.shortform = 'abk' THEN 'actor' ELSE module.shortform END AS module_code",
            "CASE WHEN xmi.shortform = 'abk' THEN 'actor' ELSE xmi.shortform END AS related_module_code",
        ];
        $qry = $this->connection()->createQueryBuilder();
        $qry->select($select)
            ->from('cor_tbl_xmi', 'tbl')
            ->leftJoin('tbl', 'cor_tbl_module', 'module', 'tbl.itemkey = module.itemkey')
            ->leftJoin('tbl', 'cor_tbl_module', 'xmi', 'tbl.xmi_itemkey = xmi.itemkey')
            ->leftJoin('tbl', 'cor_tbl_users', 'user', 'tbl.cre_by = user.id')
            ->orderBy('module_code')
            ->addOrderBy('related_module_code');

        $limit = 1000;
        $offset = 0;
        $qry->setFirstResult($offset)->setMaxResults($limit);
        while ($xmis = $qry->execute()->fetchAllAssociative()) {
            foreach ($xmis as $xmi) {
                $row = [];
                $row['module_code'] = $xmi['module_code'];
                $row['property'] = $xmi['related_module_code'];
                $row['datatype'] = 'relation';
                $row['property_label'] = '';
                $this->properties[$row['module_code']][$row['property']] = $row;
                $row['module_code'] = $xmi['related_module_code'];
                $row['property'] = $xmi['module_code'];
                $this->properties[$row['module_code']][$row['property']] = $row;
            }
            $offset += $limit;
            $qry->setFirstResult($offset)->setMaxResults($limit);
        }
    }

    private function exportProperties() : void
    {
        $header = [
            'module_code',
            'property',
            'datatype',
            'property_label',
        ];
        foreach (array_keys($this->properties) as $module_code) {
            $path = $this->path.'/module_'.$module_code.'_property.csv';
            $fp = fopen($path, 'w');
            fputcsv($fp, $header);
            ksort($this->properties[$module_code]);
            foreach ($this->properties[$module_code] as $datatype => $row) {
                fputcsv($fp, $row);
            }
            fclose($fp);
        }
    }

    private function exportAttributes() : void
    {
        $header = [
            'property',
            'attribute',
            'attribute_label',
        ];
        $select = [
            'lut.attributetype AS property',
            'attr.attribute',
            "alias.alias AS attribute_label",
        ];

        $qry = $this->connection()->createQueryBuilder();
        $qry->select($select)
            ->from('cor_lut_attributetype', 'lut')
            ->leftJoin('lut', 'cor_lut_attribute', 'attr', 'attr.attributetype = lut.id')
            ->leftJoin('lut', 'cor_tbl_alias', 'alias', "alias.itemkey = 'cor_lut_attribute' AND alias.itemvalue = attr.id AND alias.language = '$this->language'")
            ->orderBy('lut.attributetype', 'ASC')
            ->addOrderBy('attr.attributetype', 'ASC');
        $this->write($qry->getSql());

        $this->exportQuery($qry, 'attribute', $header);
    }
}
