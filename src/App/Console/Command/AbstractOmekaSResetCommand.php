<?php

/**
 * ARK Console Command.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace App\Console\Command;

use ARK\ARK;
use ARK\DBAL\Console\Command\DatabaseCommand;
use DateTime;
use Symfony\Component\HttpClient\HttpClient;

abstract class AbstractOmekaSResetCommand extends DatabaseCommand
{
    // OVERRIDE THIS
    protected $modules = ['abk'];

    // OVERRIDE THIS
    protected function configure() : void
    {
        $this->setCommandOptions('ark:reset', 'Reset ARK / Omeka S IDs');
    }

    protected function setCommandOptions(string $name, string $description) : void
    {
        $modules = implode(',', array_keys($this->modules));
        $this->setName($name)
            ->setDescription($description)
            ->addOptionBoolean('skos', 'Only rest Attributes for SKOS Items')
            ->addOptionValue('modules', 'Comma-separated list of Modules to reset', $modules);
    }

    protected function doExecute() : int
    {
        // If passed a list of modules, check they are valid and compose into correct process order
        $mods = explode(',', $this->getOption('modules'));
        foreach ($this->modules as $mod) {
            if (in_array($mod, $mods)) {
                $modules[] = $mod;
            }
        }


        $this->write('');
        $this->resetUsers();
        $this->resetAttributes();
        if ($this->getOption('skos')) {
            $this->write('');
            return 0;
        }
        foreach ($modules as $mod) {
            $this->resetModule($mod);
        }
        $this->write('');

        return 0;
    }

    protected function resetUsers() : void
    {
        if (!$this->askConfirmation('Reset Users?', false)) {
            return;
        }
        $this->write('Reset Users...');
        $this->connection()->beginTransaction();
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update('cor_tbl_users', 'users')
            ->set('users.omekas_id', ':omekas_id')
            ->set('users.omekas_updated', ':omekas_updated');
        $qbu->setParameter('omekas_id', null);
        $qbu->setParameter('omekas_updated', null);
        $update = $qbu->execute();
        $this->connection()->commit();
    }

    protected function resetAttributes() : void
    {
        if (!$this->askConfirmation('Reset Attributes / SKOS?', false)) {
            return;
        }
        $this->write('Reset Attributes...');
        $this->connection()->beginTransaction();

        // Update Attribute Type
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update('cor_lut_attributetype', 'typ')
            ->set('typ.omekas_id', ':omekas_id')
            ->set('typ.omekas_collection_id', ':omekas_collection_id')
            ->set('typ.omekas_customvocab_id', ':omekas_customvocab_id')
            ->set('typ.omekas_updated', ':omekas_updated');
        $qbu->setParameter('omekas_id', null);
        $qbu->setParameter('omekas_collection_id', null);
        $qbu->setParameter('omekas_customvocab_id', null);
        $qbu->setParameter('omekas_updated', null);
        $update = $qbu->execute();

        // Update Attribute Type
        $qba = $this->connection()->createQueryBuilder();
        $qba->update('cor_lut_attribute', 'attr')
            ->set('attr.omekas_id', ':omekas_id')
            ->set('attr.omekas_updated', ':omekas_updated');
        $qba->setParameter('omekas_id', null);
        $qba->setParameter('omekas_updated', null);
        $update = $qba->execute();

        $this->connection()->commit();
    }

    protected function resetModule(string $mod) : void
    {
        $this->write('Reset module '.$mod.' ...');
        $this->connection()->beginTransaction();

        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update($mod.'_tbl_'.$mod, 'tbl')
            ->set('tbl.omekas_id', ':omekas_id')
            ->set('tbl.omekas_updated', ':omekas_updated')
            ->setParameter('omekas_id', null)
            ->setParameter('omekas_updated', null);
        $update = $qbu->execute();

        $qbf = $this->connection()->createQueryBuilder();
        $qbf->update('cor_lut_file', 'file')
            ->where('file.id = :file')
            ->set('file.omekas_id', ':omekas_id')
            ->set('file.omekas_updated', ':omekas_updated')
            ->setParameter('omekas_id', null)
            ->setParameter('omekas_updated', null);

        $qbi = $this->connection()->createQueryBuilder();
        $qbi->select('tbl.*')
            ->from('cor_tbl_file', 'tbl')
            ->where('tbl.itemkey = :itemkey')
            ->setParameter('itemkey', $mod.'_cd');
        $files = $qbi->execute()->fetchAllAssociative();
        foreach ($files as $file) {
            $qbf->setParameter('file', $file['file']);
            $qbf->execute();
        }

        $this->connection()->commit();
    }
}
