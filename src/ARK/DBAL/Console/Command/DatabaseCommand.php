<?php

/**
 * Ark Database Console Command.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace ARK\DBAL\Console\Command;

use ARK\Console\Command\AbstractCommand;
use ARK\DBAL\Connection\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Migrations\Configuration\Configuration;
use Symfony\Component\Console\Question\ChoiceQuestion;

abstract class DatabaseCommand extends AbstractCommand
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        parent::__construct();
    }

    protected function connection() : Connection
    {
        return $this->connection;
    }

    protected function confirmCommand(string $text) : bool
    {
        $db = $this->connection->getDatabase();
        $regex = "/$db/i";
        $this->write("WARNING: $text");
        $confirm = $this->askConfirmation("To confirm, please type in the database name ($db)", false, false, $regex);
        if (!$confirm) {
            $this->write('FAILED: Database name does not match.');
        }
        return $confirm;
    }

    protected function chooseDatabase(string $text = null) : string
    {
        if (!$text) {
            $text = 'Please choose a database';
        }
        return $this->askChoice("$text : ", $this->connection->listDatabases());
    }
}
