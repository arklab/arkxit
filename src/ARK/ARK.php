<?php

/**
 * ARK Installation Globals.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace ARK;

use DateTime;
use DateTimeZone;
use Symfony\Component\Filesystem\Filesystem;

class ARK
{
    public static function timestamp() : DateTime
    {
        return new DateTime('now', new DateTimeZone('UTC'));
    }

    public static function utc(string $datetime) : DateTime
    {
        return new DateTime($datetime, new DateTimeZone('UTC'));
    }

    public static function installDir() : string
    {
        return realpath(__DIR__.'/../..');
    }

    public static function dirList(string $dir, bool $fullPath = false) : iterable
    {
        $dirs = [];
        foreach (scandir($dir) as $entry) {
            if ($entry !== '.' && $entry !== '..' && is_dir($dir.'/'.$entry)) {
                $dirs[] = $fullPath ? $dir.'/'.$entry : $entry;
            }
        }
        return $dirs;
    }

    public static function fileList(string $dir, bool $fullPath = false) : iterable
    {
        $files = [];
        foreach (scandir($dir) as $entry) {
            if ($entry !== '.' && $entry !== '..' && !is_dir($dir.'/'.$entry)) {
                $files[] = $fullPath ? $dir.'/'.$entry : $entry;
            }
        }
        return $files;
    }

    public static function pathList(string $dir, bool $fullPath = false) : iterable
    {
        $paths = [];
        foreach (scandir($dir) as $entry) {
            if ($entry !== '.' && $entry !== '..') {
                $paths[] = $fullPath ? $dir.'/'.$entry : $entry;
            }
        }
        return $paths;
    }

    public static function jsonDecodeFile(string $path) : iterable
    {
        return json_decode(file_get_contents($path), true);
    }

    public static function jsonEncodeWrite(array $data, string $path, bool $pretty = true) : int
    {
        $dir = \dirname($path);
        if (!is_dir($dir)) {
            $fs = new Filesystem();
            $fs->mkdir($dir);
        }
        return file_put_contents($path, self::jsonEncode($data, $pretty));
    }

    public static function jsonEncode(array $data, bool $pretty = true) : string
    {
        if ($pretty) {
            return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        return json_encode($data);
    }
}
