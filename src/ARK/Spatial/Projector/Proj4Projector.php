<?php

declare(strict_types=1);

namespace ARK\Spatial\Projector;

use Brick\Geo\CoordinateSystem;
use Brick\Geo\Point;
use Brick\Geo\Projector\Projector;
use proj4php\Proj;
use proj4php\Proj4php;
use proj4php\Point as Proj4Point;

/**
 * Reprojects a Point using PROJ4.
 */
final class Proj4Projector implements Projector
{
    private ?Proj4php $proj4 = null;
    private readonly int $targetSRID;
    private readonly Proj $targetProj4;
    private ?int $sourceSRID = null;
    private ?Proj $sourceProj4 = null;
    private ?CoordinateSystem $targetCS = null;

    public function __construct(int $targetSRID) {
        $this->proj4 = new Proj4php();
        $this->targetSRID = $targetSRID;
        $this->targetProj4 = new Proj('EPSG:'.$targetSRID, $this->proj4);
    }

    public function project(Point $point): Point
    {
        $sourceSRID = $point->coordinateSystem()->SRID();
        if ($sourceSRID !== $this->sourceSRID) {
            $this->sourceProj4 = new Proj('EPSG:'.$sourceSRID, $this->proj4);
            $this->sourceSRID = $sourceSRID;
            $this->targetCS = $point->coordinateSystem()->withSRID($this->targetSRID);
        }
        if ($this->targetCS->hasZ()) {
            $source = new Proj4Point($point->x(), $point->y(), $point->z(), $this->sourceProj4);
            $target = $this->proj4->transform($this->sourceProj4, $this->targetProj4, $source);
            return new Point($this->targetCS, $target->x, $target->y, $target->z);
        } else {
            $source = new Proj4Point($point->x(), $point->y(), $this->sourceProj4);
            $target = $this->proj4->transform($this->sourceProj4, $this->targetProj4, $source);
            return new Point($this->targetCS, $target->x, $target->y);
        }
    }

    public function getTargetCoordinateSystem(CoordinateSystem $sourceCoordinateSystem): CoordinateSystem
    {
        return $sourceCoordinateSystem->withSRID($this->targetSRID);
    }
}
