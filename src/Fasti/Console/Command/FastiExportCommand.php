<?php

/**
 * ARK Console Command.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace Fasti\Console\Command;

use ARK\ARK;
use App\Console\Command\AbstractOmekaSExportCommand;
use DateTime;
use Doctrine\DBAL\ArrayParameterType;
use Symfony\Component\HttpClient\HttpClient;

class FastiExportCommand extends AbstractOmekaSExportCommand
{
    protected $vocabularies = [
        'site' => [
            'site' => [
                'type' => [
                    'AIAC',
                    'CHR',
                    'FOLDER-it',
                    'FOLDER-con',
                    'FOLDER-sur',
                ],
                'collection' => 'Item Register Collection',
                'customvocab' => 'Item Register Vocabulary',
                'scheme' => 'Item Register Scheme',
                'notation' => 'item_register',
                'is_public' => 1,
                'owner' => 1,
            ],
        ],
        'action' => [
            'team_role' => [
                'source' => 'action',
                'type' => [
                    'aerialphotographs',
                    'analysesoftheprovenanceofrawmaterials',
                    'anthracologist',
                    'anthropologist',
                    'archaeobiologist',
                    'archaeobotanist',
                    'archaeologicaloperator',
                    'archaeologist',
                    'archaeologistanddraftsman',
                    'archaeologistandinformatist',
                    'archaeologistmuseumcommittee',
                    'archaeologistrepresentingthesoprintendenza',
                    'archaeologists',
                    'archaeometallurgist',
                    'archaeozoologist',
                    'archeologists',
                    'architect',
                    'architecturalhistorian',
                    'architecturalstructuralrestoration',
                    'archivialresearch',
                    'arthistorian',
                    'assistant',
                    'basketworkspecialist',
                    'bibliographicresearchandpotteryspecilist',
                    'buildingcompany',
                    'carpologist',
                    'cartographer',
                    'chemist',
                    'coinspecialist',
                    'conservator',
                    'conservatorassistant',
                    'dendrochronologist',
                    'director',
                    'draftsman',
                    'engineer',
                    'environmentalspecialist',
                    'epigraphist',
                    'ethnoarchaeologist',
                    'fabricanalystpottery',
                    'fielddirector',
                    'fielddirectorassistant',
                    'fielddirectormuseumcommettee',
                    'filmeddocumentation',
                    'financier',
                    'findsdraftsman',
                    'findsspecialist',
                    'foreman',
                    'geoarchaeologist',
                    'geographer',
                    'geologist',
                    'geophysicalsurveyandgisdirector',
                    'geophysicalsurveyor',
                    'geophysicist',
                    'georadarsurvey',
                    'georadarsurveydirector',
                    'gissystems',
                    'historian',
                    'honoraryinspector',
                    'informationtechnician',
                    'linguist',
                    'lithictechnologist',
                    'lithictypologist',
                    'machineoperator',
                    'mineralogist',
                    'mortarspecialist',
                    'mosaicandsigninumfloorsspecialist',
                    'mosaicsandpaintingspecialist',
                    'museumassistant',
                    'museumcommittee',
                    'museumdirector',
                    'museumofdeposit',
                    'numismatist',
                    'osteologist',
                    'paleethnologist',
                    'paleoanthropologist',
                    'paleobotanist',
                    'paleoecologo',
                    'paleolinguist',
                    'paleontologist',
                    'paleopathologist',
                    'palynologist',
                    'pedologist',
                    'photogrammetists',
                    'photogrammetrist',
                    'photographer',
                    'photographicsurveyor',
                    'pigmentspecialist',
                    'planner',
                    'potteryspecialist',
                    'projectmanager',
                    'radiocarbonanalysis',
                    'respforthecartadellagroandformaurbisromae',
                    'rinst',
                    'rptauthor',
                    'scientificassistant',
                    'sedimentologist',
                    'shellspecialist',
                    'sitesupervisor',
                    'speleologist',
                    'structuralanalyst',
                    'student',
                    'students',
                    'summaryauthor',
                    'supervisor',
                    'surveyor',
                    'surveyoranddraftsman',
                    'technicalassistant',
                    'thermolumspecialist',
                    'traceologist',
                    'underwaterarch',
                    'volcanologist',
                    'volcontact',
                    'volunteer',
                    'volunteers',
                    'wallplasterandpavementrestoration',
                    'woodcharcoalspecialist',
                    'woodspecialist',
                    'workdirector',
                    'worker',
                ],
                'collection' => 'Team Role Collection',
                'customvocab' => 'Team Role Vocabulary',
                'scheme' => 'Team Role Scheme',
                'notation' => 'team_role',
                'is_public' => 1,
                'owner' => 1,
            ],
        ],
        'attribute' => [
            'monument' => [
                'type' => 'monument',
                'collection' => 'Monument Type Collection',
                'customvocab' => 'Monument Type Vocabulary',
                'notation' => 'monument',
                'is_public' => 1,
                'owner' => 1,
            ],
            'approach' => [
                'type' => 'approach',
                'collection' => 'Survey Approach Collection',
                'customvocab' => 'Survey Approach Vocabulary',
                'notation' => 'survey_approach',
                'is_public' => 1,
                'owner' => 1,
            ],
            'spatsample' => [
                'type' => 'spatsample',
                'collection' => 'Spatial Sample Collection',
                'customvocab' => 'Spatial Sample Vocabulary',
                'notation' => 'spatial_sample',
                'is_public' => 1,
                'owner' => 1,
            ],
            'artefactcollection' => [
                'type' => 'artefactcollection',
                'collection' => 'Artefact Collection Collection',
                'customvocab' => 'Artefact Collection Vocabulary',
                'notation' => 'artefact_collection',
                'is_public' => 1,
                'owner' => 1,
            ],
            'collectionunit' => [
                'type' => 'collectionunit',
                'collection' => 'Artefact Collection Unit Collection',
                'customvocab' => 'Artefact Collection Unit Vocabulary',
                'notation' => 'artefact_collection_unit',
                'is_public' => 1,
                'owner' => 1,
            ],
            'locationtech' => [
                'type' => 'locationtech',
                'collection' => 'Location Technique Collection',
                'customvocab' => 'Location Technique Vocabulary',
                'notation' => 'location_technique',
                'is_public' => 1,
                'owner' => 1,
            ],
            'ssartifacts' => [
                'type' => 'ssartifacts',
                'collection' => 'Artefacts Collection',
                'customvocab' => 'Artefacts Vocabulary',
                'notation' => 'artefacts',
                'is_public' => 1,
                'owner' => 1,
            ],
            'ssenvrionment' => [
                'type' => 'ssenvrionment',
                'collection' => 'Paleoecology Collection',
                'customvocab' => 'Paleoecology Vocabulary',
                'notation' => 'paleoecology',
                'is_public' => 1,
                'owner' => 1,
            ],
            'sspeople' => [
                'type' => 'sspeople',
                'collection' => 'Demography Collection',
                'customvocab' => 'Demography Vocabulary',
                'notation' => 'demography',
                'is_public' => 1,
                'owner' => 1,
            ],
            'ssremotesensing' => [
                'type' => 'ssremotesensing',
                'collection' => 'Remote Sensing Collection',
                'customvocab' => 'Remote Sensing Vocabulary',
                'notation' => 'remote_sensing',
                'is_public' => 1,
                'owner' => 1,
            ],
        ],
    ];

    protected $core = [
        'item' => [
            'property' => 'fasti:item',
        ],
        'item_register' => [
            'property' => 'fasti:item_register',
        ],
        'item_sequence' => [
            'property' => 'fasti:item_sequence',
        ],
    ];

    protected $modules = [
        'abk' => [
            'key' => [
                'tbl' => 'abk_tbl_abk',
                'lut' => 'abk_lut_abktype',
                'mod_cd' => 'abk_cd',
                'mod_no' => 'abk_no',
                'type' => 'abktype',
            ],
            'core' => true,
            'select' => [
                'tbl' => [
                    'abk_cd' => 'item',
                    'ste_cd' => 'item_register',
                    'abk_no' => 'item_sequence',
                    'cre_on' => 'created',
                    'omekas_id' => 'id',
                    'omekas_updated' => 'updated',
                ],
                'lut' => [
                    'abktype' => 'type',
                ],
            ],
            'modtype' => [
                'people' => [
                    'resource_template_name' => 'Person',
                    'resource_class_name' => 'fasti:Person',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'name' => [
                                'property' => 'fasti:actor_name',
                            ],
                            'address_1' => [
                                'property' => 'fasti:actor_address',
                            ],
                            'telephone' => [
                                'property' => 'fasti:actor_telephone',
                            ],
                            'fax' => [
                                'property' => 'fasti:actor_fax',
                            ],
                            'email' => [
                                'property' => 'fasti:actor_email',
                            ],
                            'website' => [
                                'property' => 'fasti:actor_website',
                            ],
                            'organisation' => [
                                'property' => 'fasti:actor_organisation_name',
                            ],
                        ],
                    ],
                ],
                'organisation' => [
                    'resource_template_name' => 'Organisation',
                    'resource_class_name' => 'fasti:Organisation',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'name' => [
                                'property' => 'fasti:actor_name',
                            ],
                            'address_1' => [
                                'property' => 'fasti:actor_address',
                            ],
                            'telephone' => [
                                'property' => 'fasti:actor_telephone',
                            ],
                            'fax' => [
                                'property' => 'fasti:actor_fax',
                            ],
                            'email' => [
                                'property' => 'fasti:actor_email',
                            ],
                            'website' => [
                                'property' => 'fasti:actor_website',
                            ],
                        ],
                    ],
                ],
            ],
        ],
        'fst' => [
            'key' => [
                'tbl' => 'fst_tbl_fst',
                'mod_cd' => 'fst_cd',
                'mod_no' => 'fst_no',
                'status' => 20,
            ],
            'core' => true,
            'select' => [
                'tbl' => [
                    'fst_cd' => 'item',
                    'ste_cd' => 'item_register',
                    'fst_no' => 'item_sequence',
                    'cre_on' => 'created',
                    'omekas_id' => 'id',
                    'omekas_updated' => 'updated',
                ],
                'lut' => [
                ],
            ],
            'modtype' => [
                'fst' => [
                    'resource_template_name' => 'Site',
                    'resource_class_name' => 'fasti:Site',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'sitename' => [
                                'property' => 'fasti:site_name',
                            ],
                            'locn' => [
                                'property' => 'fasti:location_name',
                            ],
                            'astname' => [
                                'property' => 'fasti:location_name_ancient',
                            ],
                            'website' => [
                                'property' => 'fasti:site_website',
                            ],
                            'pleiades' => [
                                'property' => 'fasti:location_pleiades',
                            ],
                        ],
                        'number' => [
                            'easting' => [
                                'property' => 'fasti:location_easting',
                            ],
                            'northing' => [
                                'property' => 'fasti:location_northing',
                            ],
                        ],
                        'span' => [
                            'daterange' => [
                                'property' => 'fasti:site_date_range',
                                'spantype' => 'daterange',
                            ],
                        ],
                        'attribute' => [
                            'monument' => [
                                'property' => 'fasti:site_monument',
                            ],
                        ],
                        'file' => [
                            'images' => [
                                'property' => 'fasti:media_image',
                            ],
                            'videos' => [
                                'property' => 'fasti:media_video',
                            ],
                        ],
                        'custom' => [
                            'geolocation' => [
                                'property' => 'fasti:location_geolocation',
                                'method' => 'processGeolocation',
                            ],
                            'live' => [
                                'method' => 'processLiveStatus',
                                'attributetype' => 'recflag',
                                'attribute' => ['cangolive', 'cangolivecon'],
                            ],
                        ],
                        'geonames' => [
                            'country' => [
                                'property' => 'fasti:location_country',
                            ],
                            'admin1' => [
                                'property' => 'fasti:location_admin1',
                            ],
                            'admin2' => [
                                'property' => 'fasti:location_admin2',
                            ],
                            'admin3' => [
                                'property' => 'fasti:location_admin3',
                            ],
                        ],
                    ],
                ],
            ],
        ],
        'sea' => [
            'key' => [
                'tbl' => 'sea_tbl_sea',
                'lut' => 'sea_lut_seatype',
                'mod_cd' => 'sea_cd',
                'mod_no' => 'sea_no',
                'type' => 'seatype',
            ],
            'core' => true,
            'select' => [
                'tbl' => [
                    'sea_cd' => 'item',
                    'ste_cd' => 'item_register',
                    'sea_no' => 'item_sequence',
                    'cre_on' => 'created',
                    'omekas_id' => 'id',
                    'omekas_updated' => 'updated',
                ],
                'lut' => [
                    'seatype' => 'type',
                ],
            ],
            'modtype' => [
                'archaeology' => [
                    'resource_template_name' => 'Excavation',
                    'resource_class_name' => 'fasti:Excavation',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'summary' => [
                                'property' => 'fasti:season_summary',
                                'language' => true,
                            ],
                        ],
                        'date' => [
                            'dateofintervention' => [
                                'property' => 'fasti:season_year',
                                'format' => 'Y',
                            ],
                        ],
                        'action' => [
                            'summaryauthor' => [
                                'property' => 'fasti:season_summary_author',
                            ],
                            'director' => [
                                'property' => 'fasti:season_director',
                            ],
                            'rinst' => [
                                'property' => 'fasti:season_research_body',
                            ],
                            'finst' => [
                                'property' => 'fasti:season_funding_body',
                            ],
                            'aerialphotographs' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'analysesoftheprovenanceofrawmaterials' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'anthracologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'anthropologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeobiologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeobotanist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologicaloperator' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistanddraftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistandinformatist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistmuseumcommittee' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistrepresentingthesoprintendenza' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologists' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeometallurgist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeozoologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archeologists' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'architect' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'architecturalhistorian' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'architecturalstructuralrestoration' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archivialresearch' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'arthistorian' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'assistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'basketworkspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'bibliographicresearchandpotteryspecilist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'buildingcompany' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'carpologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'cartographer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'chemist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'coinspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'conservator' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'conservatorassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'dendrochronologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'draftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'engineer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'environmentalspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'epigraphist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'ethnoarchaeologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fabricanalystpottery' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fielddirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fielddirectorassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fielddirectormuseumcommettee' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'filmeddocumentation' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'financier' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'findsdraftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'findsspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'foreman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geoarchaeologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geographer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geophysicalsurveyandgisdirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geophysicalsurveyor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geophysicist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'georadarsurvey' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'georadarsurveydirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'gissystems' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'historian' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'honoraryinspector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'informationtechnician' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'linguist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'lithictechnologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'lithictypologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'machineoperator' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mineralogist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mortarspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mosaicandsigninumfloorsspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mosaicsandpaintingspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumcommittee' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumdirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumofdeposit' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'numismatist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'osteologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleethnologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleoanthropologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleobotanist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleoecologo' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleolinguist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleontologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleopathologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'palynologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'pedologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photogrammetists' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photogrammetrist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photographer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photographicsurveyor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'pigmentspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'planner' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'potteryspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'projectmanager' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'radiocarbonanalysis' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'respforthecartadellagroandformaurbisromae' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'rptauthor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'scientificassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'sedimentologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'shellspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'sitesupervisor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'speleologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'structuralanalyst' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'student' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'students' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'supervisor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'surveyor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'surveyoranddraftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'technicalassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'thermolumspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'traceologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'underwaterarch' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volcanologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volcontact' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volunteer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volunteers' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'wallplasterandpavementrestoration' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'woodcharcoalspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'woodspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'workdirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'worker' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                        ],
                        'xmi' => [
                            'fstxmisea' => [
                                'property' => 'fasti:season_site',
                                'module' => 'fst',
                            ],
                        ],
                        'file' => [
                            'images' => [
                                'property' => 'fasti:media_image',
                            ],
                            'videos' => [
                                'property' => 'fasti:media_video',
                            ],
                        ],
                        'custom' => [
                            'live' => [
                                'method' => 'processLiveStatus',
                                'attributetype' => 'recflag',
                                'attribute' => ['cangolive'],
                            ],
                        ],
                    ],
                ],
                'conservation' => [
                    'resource_template_name' => 'Conservation',
                    'resource_class_name' => 'fasti:Conservation',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'summary' => [
                                'property' => 'fasti:season_summary',
                                'language' => true,
                            ],
                        ],
                        'date' => [
                            'dateofintervention' => [
                                'property' => 'fasti:season_year',
                                'format' => 'Y',
                            ],
                        ],
                        'action' => [
                            'summaryauthor' => [
                                'property' => 'fasti:season_summary_author',
                            ],
                            'director' => [
                                'property' => 'fasti:season_director',
                            ],
                            'rinst' => [
                                'property' => 'fasti:season_research_body',
                            ],
                            'finst' => [
                                'property' => 'fasti:season_funding_body',
                            ],
                            'aerialphotographs' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'analysesoftheprovenanceofrawmaterials' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'anthracologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'anthropologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeobiologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeobotanist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologicaloperator' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistanddraftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistandinformatist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistmuseumcommittee' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologistrepresentingthesoprintendenza' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeologists' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeometallurgist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archaeozoologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archeologists' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'architect' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'architecturalhistorian' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'architecturalstructuralrestoration' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'archivialresearch' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'arthistorian' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'assistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'basketworkspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'bibliographicresearchandpotteryspecilist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'buildingcompany' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'carpologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'cartographer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'chemist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'coinspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'conservator' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'conservatorassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'dendrochronologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'draftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'engineer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'environmentalspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'epigraphist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'ethnoarchaeologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fabricanalystpottery' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fielddirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fielddirectorassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'fielddirectormuseumcommettee' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'filmeddocumentation' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'financier' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'findsdraftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'findsspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'foreman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geoarchaeologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geographer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geophysicalsurveyandgisdirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geophysicalsurveyor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'geophysicist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'georadarsurvey' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'georadarsurveydirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'gissystems' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'historian' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'honoraryinspector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'informationtechnician' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'linguist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'lithictechnologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'lithictypologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'machineoperator' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mineralogist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mortarspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mosaicandsigninumfloorsspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'mosaicsandpaintingspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumcommittee' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumdirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'museumofdeposit' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'numismatist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'osteologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleethnologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleoanthropologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleobotanist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleoecologo' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleolinguist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleontologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'paleopathologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'palynologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'pedologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photogrammetists' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photogrammetrist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photographer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'photographicsurveyor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'pigmentspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'planner' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'potteryspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'projectmanager' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'radiocarbonanalysis' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'respforthecartadellagroandformaurbisromae' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'rptauthor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'scientificassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'sedimentologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'shellspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'sitesupervisor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'speleologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'structuralanalyst' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'student' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'students' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'supervisor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'surveyor' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'surveyoranddraftsman' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'technicalassistant' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'thermolumspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'traceologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'underwaterarch' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volcanologist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volcontact' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volunteer' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'volunteers' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'wallplasterandpavementrestoration' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'woodcharcoalspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'woodspecialist' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'workdirector' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                            'worker' => [
                                'property' => 'fasti:season_team',
                                'annotation' => [
                                    'resource_template_name' => 'Actor Role Annotation',
                                    'property' => 'fasti:season_role',
                                ],
                            ],
                        ],
                        'xmi' => [
                            'fstxmisea' => [
                                'property' => 'fasti:season_site',
                                'module' => 'fst',
                            ],
                        ],
                        'file' => [
                            'images' => [
                                'property' => 'fasti:media_image',
                            ],
                            'videos' => [
                                'property' => 'fasti:media_video',
                            ],
                        ],
                        'custom' => [
                            'live' => [
                                'method' => 'processLiveStatus',
                                'attributetype' => 'recflag',
                                'attribute' => ['cangolivecon'],
                            ],
                        ],
                    ],
                ],
                'chronique' => [
                    'resource_template_name' => 'Chronique',
                    'resource_class_name' => 'fasti:Season',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'summary' => [
                                'property' => 'fasti:season_summary',
                                'language' => true,
                            ],
                            'chroniquelink' => [
                                'property' => 'fasti:season_source',
                            ],
                        ],
                        'date' => [
                            'dateofintervention' => [
                                'property' => 'fasti:season_year',
                                'format' => 'Y',
                            ],
                        ],
                        'action' => [
                            'summaryauthor' => [
                                'property' => 'fasti:season_summary_author',
                            ],
                            'rinst' => [
                                'property' => 'fasti:season_research_body',
                            ],
                        ],
                        'xmi' => [
                            'fstxmisea' => [
                                'property' => 'fasti:season_site',
                                'module' => 'fst',
                            ],
                        ],
                        'custom' => [
                            'live' => [
                                'method' => 'processLiveStatus',
                                'attributetype' => 'recflag',
                                'attribute' => ['cangolive'],
                            ],
                        ],
                    ],
                ],
            ],
        ],
        'sur' => [
            'key' => [
                'tbl' => 'sur_tbl_sur',
                'mod_cd' => 'sur_cd',
                'mod_no' => 'sur_no',
            ],
            'core' => true,
            'select' => [
                'tbl' => [
                    'sur_cd' => 'item',
                    'ste_cd' => 'item_register',
                    'sur_no' => 'item_sequence',
                    'cre_on' => 'created',
                    'omekas_id' => 'id',
                    'omekas_updated' => 'updated',
                ],
                'lut' => [
                ],
            ],
            'modtype' => [
                'sur' => [
                    'resource_template_name' => 'Survey',
                    'resource_class_name' => 'fasti:Survey',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'surname' => [
                                'property' => 'fasti:survey_name',
                            ],
                            'locn' => [
                                'property' => 'fasti:location_name',
                            ],
                            'astname' => [
                                'property' => 'fasti:location_name_ancient',
                            ],
                            'pleiades' => [
                                'property' => 'fasti:location_pleiades',
                            ],
                            'projdesc' => [
                                'property' => 'fasti:survey_description',
                            ],
                            'website' => [
                                'property' => 'fasti:survey_website',
                            ],
                            'surfundinst' => [
                                'property' => 'fasti:survey_funding_body',
                            ],
                            'surresinst' => [
                                'property' => 'fasti:survey_research_body',
                            ],
                            'meth_other' => [
                                'property' => 'fasti:survey_method_other',
                            ],
                            'pri_question' => [
                                'property' => 'fasti:survey_primary_question',
                            ],
                            'ss_other' => [
                                'property' => 'fasti:survey_comments',
                            ],
                            'surbibtxt' => [
                                'property' => 'fasti:survey_bibliography_text',
                            ],
                        ],
                        'number' => [
                            'easting' => [
                                'property' => 'fasti:location_easting',
                            ],
                            'northing' => [
                                'property' => 'fasti:location_northing',
                            ],
                        ],
                        'span' => [
                            'projdates' => [
                                'property' => 'fasti:survey_project_date_range',
                                'spantype' => 'projdates',
                            ],
                            'daterange' => [
                                'property' => 'fasti:survey_date_range',
                                'spantype' => 'daterange',
                            ],
                        ],
                        'attribute' => [
                            'approach' => [
                                'property' => 'fasti:survey_approach',
                            ],
                            'spatsample' => [
                                'property' => 'fasti:survey_spatial_sampling',
                            ],
                            'artefactcollection' => [
                                'property' => 'fasti:survey_artefact_collection',
                            ],
                            'collectionunit' => [
                                'property' => 'fasti:survey_artefact_collection_unit',
                            ],
                            'locationtech' => [
                                'property' => 'fasti:survey_location_technique',
                            ],
                            'ssartifacts' => [
                                'property' => 'fasti:survey_artefacts',
                            ],
                            'ssenvrionment' => [
                                'property' => 'fasti:survey_paleoecology',
                            ],
                            'sspeople' => [
                                'property' => 'fasti:survey_demography',
                            ],
                            'ssremotesensing' => [
                                'property' => 'fasti:survey_remote_sensing',
                            ],
                        ],
                        'action' => [
                            'director' => [
                                'property' => 'fasti:survey_director',
                            ],
                        ],
                        'spatial' => [
                            'area_geometry' => [
                                'property' => 'fasti:survey_area_geometry',
                                'geomtype' => 2,
                                'datatype' => 'geom',
                                'geometry' => 'Polygon',
                                'srid' => 900913,
                            ],
                            'area_bbox' => [
                                'property' => 'fasti:survey_area_bbox',
                                'geomtype' => 2,
                                'datatype' => 'bbox',
                                'geometry' => 'Polygon',
                                'srid' => 900913,
                            ],
                            'unit_geometry' => [
                                'property' => 'fasti:survey_unit_geometry',
                                'geomtype' => 1,
                                'datatype' => 'geom',
                                'geometry' => 'Polygon',
                                'srid' => 900913,
                            ],
                            'unit_bbox' => [
                                'property' => 'fasti:survey_unit_bbox',
                                'geomtype' => 1,
                                'datatype' => 'bbox',
                                'geometry' => 'Polygon',
                                'srid' => 900913,
                            ],
                        ],
                        'file' => [
                            'images' => [
                                'property' => 'fasti:media_image',
                            ],
                            'videos' => [
                                'property' => 'fasti:media_video',
                            ],
                            'surveydata' => [
                                'property' => 'fasti:survey_data',
                            ],
                        ],
                        'custom' => [
                            'geolocation' => [
                                'property' => 'fasti:location_geolocation',
                                'method' => 'processGeolocation',
                            ],
                            'live' => [
                                'method' => 'processLiveStatus',
                                'attributetype' => 'recflag',
                                'attribute' => ['cangolivesur'],
                            ],
                        ],
                        'geonames' => [
                            'country' => [
                                'property' => 'fasti:location_country',
                            ],
                            'admin1' => [
                                'property' => 'fasti:location_admin1',
                            ],
                            'admin2' => [
                                'property' => 'fasti:location_admin2',
                            ],
                            'admin3' => [
                                'property' => 'fasti:location_admin3',
                            ],
                        ],
                    ],
                ],
            ],
        ],
        'bib' => [
            'key' => [
                'tbl' => 'bib_tbl_bib',
                'mod_cd' => 'bib_cd',
                'mod_no' => 'bib_no',
            ],
            'core' => true,
            'select' => [
                'tbl' => [
                    'bib_cd' => 'item',
                    'ste_cd' => 'item_register',
                    'bib_no' => 'item_sequence',
                    'cre_on' => 'created',
                    'omekas_id' => 'id',
                    'omekas_updated' => 'updated',
                ],
                'lut' => [
                ],
            ],
            'modtype' => [
                'bib' => [
                    'resource_template_name' => 'Bibliography',
                    'resource_class_name' => 'fasti:Bibliography',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'quickname' => [
                                'property' => 'fasti:bibliography_citation',
                            ],
                            'bibliofulloldstyle' => [
                                'property' => 'fasti:bibliography_reference',
                            ],
                        ],
                        'xmi' => [
                            'fstxmibib' => [
                                'property' => 'fasti:bibliography_site',
                                'module' => 'fst',
                            ],
                            'bibxmisur' => [
                                'property' => 'fasti:bibliography_survey',
                                'module' => 'sur',
                            ],
                        ],
                    ],
                ],
            ],
        ],
        'rpt' => [
            'key' => [
                'tbl' => 'rpt_tbl_rpt',
                'mod_cd' => 'rpt_cd',
                'mod_no' => 'rpt_no',
            ],
            'core' => false,
            'select' => [
                'tbl' => [
                    'rpt_cd' => 'item',
                    'ste_cd' => 'item_register',
                    'rpt_no' => 'item_sequence',
                    'cre_on' => 'created',
                    'omekas_id' => 'id',
                    'omekas_updated' => 'updated',
                    'filename' => 'filename',
                    'media_id' => 'media_id',
                    'media_updated' => 'media_updated',
                ],
                'lut' => [
                ],
            ],
            'modtype' => [
                'rpt' => [
                    'resource_template_name' => 'FOLD&R Publication',
                    'resource_class_name' => 'fasti:Publication',
                    'item_sets' => [],
                    'sites' => [],
                    'datatype' => [
                        'txt' => [
                            'reporttitle' => [
                                'property' => 'fasti:publication_title',
                            ],
                            'abstract' => [
                                'property' => 'fasti:publication_abstract',
                            ],
                            'doi' => [
                                'property' => 'fasti:item_doi',
                            ],
                        ],
                        'date' => [
                            'publishedon' => [
                                'property' => 'fasti:publication_year',
                                'format' => 'Y',
                            ],
                        ],
                        'number' => [
                            'folderseriesnumber' => [
                                'property' => 'fasti:publication_series_number',
                            ],
                        ],
                        'action' => [
                            'rptauthor' => [
                                'property' => 'fasti:publication_author',
                            ],
                        ],
                        'xmi' => [
                            'seaxmirpt' => [
                                'property' => 'fasti:publication_season',
                                'module' => 'sea',
                            ],
                            'surxmirpt' => [
                                'property' => 'fasti:publication_survey',
                                'module' => 'sur',
                            ],
                        ],
                        'custom' => [
                            'series' => [
                                'property' => 'fasti:publication_series',
                                'method' => 'processFolderSeries',
                            ],
                            'document' => [
                                'property' => 'fasti:publication_document',
                                'method' => 'processFolderDocument',
                            ],
                            'live' => [
                                'method' => 'processLiveStatus',
                                'attributetype' => 'recflag',
                                'attribute' => ['cangolive'],
                            ],
                        ],
                        'media' => [
                            'document' => [
                                'method' => 'uploadMediaFolder',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];
    protected $series = [];

    protected function configure() : void
    {
        $this->setCommandOptions('fasti:export', 'Export Fasti to Omeka S API');
    }

    // Custom config for Folder
    protected function loadCustom() : void
    {
        // Fetch the Folder Series Items
        $query = [
            'resource_class_id' => $this->omekas['resource_classes']['fasti:Series']['o:id'],
            'per_page'=>10000,
        ];
        $items = $this->getOmekaSResources('items', $query)->toArray();
        foreach ($items as $series) {
            if (array_key_exists('fasti:item_register', $series)) {
                $series_code = $series['fasti:item_register'][0]['display_title'];
                $this->series[$series_code] = $series;
            }
        }
    }

    // Process the custom Geolocation fields
    protected function processGeolocation(array &$item, string $mod, string $modtype, string $field, array $map) : void
    {
        // Fetch the Site Geolocation
        $geolocation = $this->fetchGeolocation($mod, $item['item']);
        if (array_key_exists('error', $geolocation)) {
            $this->write('Site '.$item['item'].' : '.$geolocation['error']);
            return;
        }
        $easting = $geolocation['easting'];
        $northing = $geolocation['northing'];

        // Map the Geolocation field, expects lat:lon order
        $value = $map['value'];
        if (array_key_exists('o:data_type', $map) && count($map['o:data_type'][0]) > 0 && $map['o:data_type'][0] === 'geography:coordinates') {
            // geography:coordinates expects lat:lon order
            $value['@value'] = "$northing,$easting";
        } else {
            // Otherwise assume geography/geometry wants wkt
            $value['@value'] = "POINT ($easting $northing)";
        }
        $item['values'][$map['property']][] = $value;

        // Query for the Geonames fields
        $q = [
            'lat' => $northing,
            'lng' => $easting,
        ];
        $geonames = $this->getGeonames('/extendedFindNearbyJSON', $q)->toArray();
        if (array_key_exists('geonames', $geonames)) {
            foreach ($geonames['geonames'] as $geoname) {
                switch ($geoname['fcode']) {
                    case 'PCLI':
                        $this->mapGeonameValue($item, $geoname, $mod, $modtype, 'country');
                        break;
                    case 'ADM1':
                        $this->mapGeonameValue($item, $geoname, $mod, $modtype, 'admin1');
                        break;
                    case 'ADM2':
                        $this->mapGeonameValue($item, $geoname, $mod, $modtype, 'admin2');
                        break;
                    case 'ADM3':
                        $this->mapGeonameValue($item, $geoname, $mod, $modtype, 'admin3');
                        break;
                }
            }
        }
    }

    // Process the custom Geolocation fields
    protected function fetchGeolocation(string $mod, string $item) : array
    {
        $result = [];

        // Get the Site Name for Label
        $label = $mod === 'sur' ? 'surname' : 'sitename';
        $names = $this->fetchAllFragsForTypes($mod, $item, 'txt', [$label]);
        $name = ($names) ? $name = $names[0]['txt'] : null;

        // Get the Easting/Northing
        $frags = $this->fetchAllFragsForTypes($mod, $item, 'number', ['easting', 'northing']);

        if (count($frags) == 0) {
            $result['error'] = 'NO GEOLOCATION FRAGS';
        } elseif (count($frags) != 2) {
            $result['error'] = 'INCORRECT COUNT GEOLOCATION FRAGS';
        } else {
            $easting = null;
            $northing = null;
            foreach ($frags as $frag) {
                if ($frag['type'] == 'easting') {
                    $easting = $frag['number'];
                }
                if ($frag['type'] == 'northing') {
                    $northing = $frag['number'];
                }
            }
            if ($easting === null || $northing === null) {
                $result['error'] = 'MISSING GEOLOCATION FRAGS';
            } elseif ($northing > 90 || $northing < -90 || $easting > 180 || $easting < -180) {
                $result['error'] = 'INVALID GEOLOCATION VALUES';
            } else {
                $result['easting'] = $easting;
                $result['northing'] = $northing;
                $result['name'] = $name;
            }
        }
        return $result;
    }

    // Map the custom Folder Series field
    protected function processFolderSeries(array &$item, string $mod, string $modtype, string $field, array $map) : void
    {
        // Set the Series Item
        $series_code = $item['item_register'];
        $series = $this->series[$series_code];
        $this->mapResourceValue($item, $map, $series['o:id']);
    }

    // Map the custom Folder Document field
    protected function processFolderDocument(array &$item, string $mod, string $modtype, string $field, array $map) : void
    {
        // Assume only Media attached is the document
        if (array_key_exists('media_id', $item) && $item['media_id'] !== NULL) {
            $this->mapResourceValue($item, $map, $item['media_id']);
            $item['values']['o:media'][] = ['o:id' => $item['media_id']];
        }
    }

    // Map the Live status to Is Public
    protected function processLiveStatus(array &$item, string $mod, string $modtype, string $field, array $map) : void
    {
        $qbf = $this->connection()->createQueryBuilder();
        $qbf->select("tbl.boolean")
            ->from('cor_tbl_attribute', 'tbl')
            ->leftJoin('tbl', 'cor_lut_attribute', 'attr', "tbl.attribute = attr.id")
            ->leftJoin('tbl', 'cor_lut_attributetype', 'lut', "attr.attributetype = lut.id")
            ->where('tbl.itemkey = :itemkey AND tbl.itemvalue = :itemvalue AND tbl.boolean = 1 AND attr.attribute IN (:attribute) AND lut.attributetype = :attributetype')
            ->setParameter('itemkey', $this->modules[$mod]['key']['mod_cd'])
            ->setParameter('itemvalue', $item['item'])
            ->setParameter('attribute', $map['attribute'], ArrayParameterType::STRING)
            ->setParameter('attributetype', $map['attributetype']);
        $live = $qbf->execute()->fetchAssociative();
        $item['values']['o:is_public'] = (bool) $live;
    }

    // Process all Folder files for Item
    protected function uploadMediaFolder(array &$item, string $mod, string $modtype, string $field, array $custom) : int|bool
    {
        $map = $this->modules[$mod]['modtype'][$modtype]['datatype']['custom'][$field] ?? [];
        if (empty($map) || $item['filename'] === NULL || $item['media_id'] !== NULL) {
            return 0;
        }
        $mapping = $this->modules[$mod]['modtype'][$modtype];
        $mod_cd = $this->modules[$mod]['key']['mod_cd'];

        $data = [];
        if ($item['media_id'] !== null) {
            $data['o:id'] = $item['media_id'];
        }
        $data['o:item']['o:id'] = $item['id'];
        $data['o:is_public'] = $mapping['is_public'];  // TODO: Get from record flag if update?
        $data['o:owner']['o:id'] = $mapping['owner'];
        $data['o:ingester'] = 'upload';
        $data['o:source'] = $item['filename'];
        $data['file_index'] = 0;

        $path = $this->path.'/data/folder/'.$item['filename'];
        $handle = fopen($path, 'r');
        if ($handle === false) {
            $this->write('fopen failed for path '.$path);
            return false;
        }
        $result = $this->postMedia($data, [$handle]);
        fclose($handle);

        if ($result === false) {
            return false;
        }

        // If Post successful, update the Item with result
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update($mod.'_tbl_'.$mod, 'tbl')
            ->where($mod.'_cd = :id')
            ->set('tbl.media_id', ':media_id')
            ->set('tbl.media_updated', ':media_updated')
            ->setParameter('id', $item['item'])
            ->setParameter('media_id', $result['o:id'])
            ->setParameter('media_updated', $this->omekaSDateToTimestamp($result['o:modified']['@value']));
        $update = $qbu->execute();

        // Map the Omeka item property value
        $this->mapResourceValue($item, $map, $result['o:id']);

        return 1;
    }
}
