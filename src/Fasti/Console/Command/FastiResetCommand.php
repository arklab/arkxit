<?php

/**
 * ARK Console Command.
 *
 * Copyright (C) 2018-2022  L - P : Heritage LLP.
 * Copyright (C) 2022-2024  Museum of London Archaeology.
 *
 * This file is part of ARK, the Archaeological Recording Kit.
 *
 * ARK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ARK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     John Layt <jlayt@mola.org.uk>
 * @copyright  2024 Museum of London Archaeology.
 * @license    AGPL-3.0+
 */

namespace Fasti\Console\Command;

use ARK\ARK;
use App\Console\Command\AbstractOmekaSResetCommand;
use DateTime;
use Symfony\Component\HttpClient\HttpClient;

class FastiResetCommand extends AbstractOmekaSResetCommand
{
    protected $modules = ['abk', 'fst', 'sea', 'sur', 'bib', 'rpt'];

    protected function configure() : void
    {
        $this->setCommandOptions('fasti:reset', 'Reset Fasti / Omeka S IDs');
    }

    protected function resetModule(string $mod) : void
    {
        parent::resetModule($mod);

        if ($mod !== 'rpt') {
            return;
        }

        // Reset the custom folder media ID0s
        $this->connection()->beginTransaction();
        $qbu = $this->connection()->createQueryBuilder();
        $qbu->update($mod.'_tbl_'.$mod, 'tbl')
            ->set('tbl.media_id', ':media_id')
            ->set('tbl.media_updated', ':media_updated')
            ->setParameter('media_id', null)
            ->setParameter('media_updated', null);
        $qbu->execute();
        $this->connection()->commit();
    }
}
