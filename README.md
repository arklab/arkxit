# ARKxit

ARKxit is a collection of command line scripts to assist with data migrations from ARK v1 (https://gitlab.com/arklab).

These scripts are provided on an as-is best efforts basis. You should always validate for yourself that the resulting output is complete, accurate, and meets your requirements.

The output of these scripts are based solely on the contents of the database, they do not currently use the PHP schema config files. Some schema elements such as chains are only fully defined in the old PHP 5 scripts and cannot be reliably accessed. You should always check if your install is affected by these limitations.

Future plans include a JSON Schema based export of the schema and data in a format more compatible with modern Linked Data systems and better suited for archival data reuse.

## Requirements

ARKxit requires a working installation of PHP v8.2 and Composer v2 (https://getcomposer.org/).

## Installation

Clone this git repo and create a local environment file.

```
git clone git@gitlab.com:arklab/arkxit.git
cd arkxit
cp .env .env.local
```

Edit the .env.local file to set your DATABASE_URL, replacing username, password, server and database as appropriate:

```
DATABASE_URL="pdo-mysql://username:password@server:3306/database?charset=utf8mb4"
```

Install the required composer dependencies, then check that the install is working.

```
composer install
./bin/console
```

## Use

The following commands are currently available in the console:

```
ark
 ark:site:info                              Analyse the migration mapping for an ARK 1 site
 ark:export:csv                             Export an ARK 1 site tables in flat CSV format
```

Run the commands using the console.

```
./bin/console ark:site:info
```

## Commands

### ark:site:info

A command to show a summary of your ARK v1 install. This reads your database and outputs summary tables of data held.

### ark:export:csv

A command to export your database in CSV format. This closely replicates the internal database format, so while largely complete may not necessarily be the most useful for future data reuse. It should however be sufficient for long-term archival storage if properly documented.

The command saves the data exports in the exports folder in a subfolder with the database name.
* module.csv - the list of configured modules
* module_\<mod\>_item.csv - the list of item types used in the given module (only for modules that use modtype)
* module_\<mod\>_property.csv - the list of module properties in the given module (as saved in the database)
* user.csv - the list of users
* site.csv - the list of sites recorded in the database
* item_\<mod\>.csv - the data items for the given module
* property_\<datatype\>.csv - the data properties of the given datatype (e.g. date, text, etc)

Caveats:
* Chains. If your configuration includes more than 1 level of chains, or chains that are not actions and dates, then these will not be included in the export. Note that exporting datatypes with chained links is significantly slower than normal datatypes.
* Translations. The metadata exports use English as the language for labels. This can be modified in the code. Support for language selection may be added later.
